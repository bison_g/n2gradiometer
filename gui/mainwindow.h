#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <../ecbackend/gradiometer_ipc.h>
#include <QMainWindow>
#include <QFile>
#include <QTabBar>
#include <QtWidgets>

#include "ui_mainwindow.h"

#include <iostream>
#include <vector>
#include <string>
#include <unistd.h>
#include <array>
#include <QTextStream>
#include <QFileDialog>
#include <QDir>
#include <QMessageBox>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT
    QTimer *timer;
    ipc_workdata_t* pwd;
    int64_t samplecounter = 0;
    bool doplot=false;


public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void clearData();


public slots:

    void periodictimer();

    void Average();

   // void WriteFile();

    bool InitFile();
    void AppendFile();
    void CloseFile();


private slots:


    void on_clear_clicked();

    void on_go_clicked();

//    void on_single_clicked();

    void on_Browse_clicked();

  //  void on_pushButton_PID_SetParameters_clicked(bool checked);

//    void on_box_one_valueChanged(const QString &arg1);

    void on_emergency_stop_button_clicked();



    void on_pbmvstart_clicked();

    void on_chooseBackgroundFileButton_clicked();

 //   void on_startpos_valueChanged(int arg1);
 //   void on_startpos_valueChanged(const QString &arg1);

    void on_chooseBackgroundFileButton_2_clicked();

    void on_NewRunButton_clicked();

    void on_pbsave_clicked();

private:
    Ui::MainWindow *ui;
    void onlineAnalysis();
    bool SetRunDirectory(bool increase);

    QVector<double> xvalues, yvalues;
    QVector<double> avx, avy;


    int loops;
    int rep;
    int graphcounter;
    int count;

    double goal;

    QString movemessage;


    enum movestate_t {
      idle,
      setgoal1initial,
      onthemove1initial,
      setgoal,
      onthemovemeasure,
      setstart,
      movetostart,
      abort,
      abortdecelerate,
      finish
    } movestate;

    double xmin;
    double xmax;
    double ymin;
    double ymax;

    bool firstpoint;

    double xval;
    double yval_1;
    double yval_2;
    double diff;
    double starting_value;
    float rounded_speed;
    double pos1;
    double pos2;
    double startspeed;
    double measspeed;
    double accelerationtime;

    double startpos;
    //

    double ysum;
    double xsum;

    QString file_name;
    QFile outputfile;
    QString backgroundFilename;
    QString onlineAnalysisFilename = "../gradiometer_online_analysis.py";

    int64_t ix;
    sampledata_t* currentsample;

    QCPItemStraightLine *maxline;
    QCPItemStraightLine *minline;
    QCPItemText *maxtext;
    QCPItemText *mintext;
    QCPItemText *pptext;

    QVector<QVector<double>> data_matrix;
    int64_t data_matrix_ix;

    QVector<double> data;
    QVector<double> mean_x;
    QVector<double> mean_y;
    QVector<QVector<double>> average_single;
    QVector<double> average_x;
    QVector<QString> times;


    QDateTime start_time;
    QDateTime stop_time;
    qint64 current_time;
    QString start_time_string;
    QString stop_time_string;

    bool metadatasaved;

};
#endif // MAINWINDOW_H

