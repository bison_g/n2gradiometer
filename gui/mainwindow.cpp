﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <../ecbackend/gradiometer_ipc.h>
#include <iostream>
#include <vector>
#include <string>
#include <unistd.h>
#include <array>
#include <QTextStream>
#include <QFileDialog>
//#include <QDir>
#include <QMessageBox>
#include <QTime>
#include <cstdlib>

#include <QJsonParseError>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

using namespace std;

#define SERVODISTANCESCALE (2097152./4)
#define CALIBRATIONCONSTANT (3.36) // Volt per nT

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->tabWidget->setCurrentWidget(ui->tab_GradCtrl); //open Window on Gradiometer Control Tab
    timer = new QTimer(this);
    pwd   = new ipc_workdata_t;

    // setup signal and slot
    connect(timer, SIGNAL(timeout()),
            this, SLOT(periodictimer()));

    //    connect(timer, SIGNAL(on_go_clicked()), this, SLOT(WriteData()));


    // msec
    timer->start(round(1000. / 10));

    IPC_init(pwd, 0);
    IPC_read_cfd(pwd);

    ui->sbrunnumber->setValue(pwd->cfd.currentrun);

    //init all kp and ki and set arrays of spinBoxes and labels
/*
    QDoubleSpinBox *t[] = {ui->doubleSpinBox_kp1_0, ui->doubleSpinBox_kp1_1,ui->doubleSpinBox_kp1_2,ui->doubleSpinBox_kp1_3,ui->doubleSpinBox_kp1_4,ui->doubleSpinBox_kp1_5};
    QDoubleSpinBox *ti[] = {ui->doubleSpinBox_ki_0, ui->doubleSpinBox_ki_1,ui->doubleSpinBox_ki_2,ui->doubleSpinBox_ki_3,ui->doubleSpinBox_ki_4,ui->doubleSpinBox_ki_5};

    QLabel *l[] = {ui->label_kp_0, ui->label_kp_1,ui->label_kp_2,ui->label_kp_3,ui->label_kp_4,ui->label_kp_5};
    QLabel *li[] = {ui->label_ki_0, ui->label_ki_1,ui->label_ki_2,ui->label_ki_3,ui->label_ki_4,ui->label_ki_5};

    for (unsigned long int i=0; i<sizeof(t)/sizeof(*t); i++ ) {
        kp_spinBoxes[i]= t[i];
        ki_spinBoxes[i]= ti[i];
        kp_labels[i] = l[i];
        ki_labels[i] =li[i];


    }

    for (unsigned long int i=0; i<sizeof(t)/sizeof(*t); i++ ) {
        kp_spinBoxes[i]->setValue(pwd->cfd.kp_p[i]);
        ki_spinBoxes[i]->setValue(pwd->cfd.ki_p[i]);
        kp_spinBoxes[i]->setDecimals(4);
        ki_spinBoxes[i]->setDecimals(4);
        //kp_spinBoxes[i]->(4);

    }
*/
    ui->Plot->xAxis->setLabel("Motorposition (cm)");
    ui->Plot->yAxis->setLabel("Magnetic Field (pT)");

    maxline = new QCPItemStraightLine( ui->Plot);
    minline = new QCPItemStraightLine( ui->Plot);

    maxtext = new QCPItemText(ui->Plot);
    mintext = new QCPItemText(ui->Plot);
    pptext  = new QCPItemText(ui->Plot);

    QPen pen;
    pen.setColor(Qt::gray);

    maxline->setPen(pen);
    minline->setPen(pen);

    maxline->point1->setCoords(1,3.3);
    maxline->point2->setCoords(2,3.3);
    minline->point1->setCoords(1,1.3);
    minline->point2->setCoords(2,1.3);

    maxtext->setPositionAlignment(Qt::AlignLeft|Qt::AlignTop);
    maxtext->position->setCoords(-1.0, 1);
    maxtext->setText("max");
    maxtext->setTextAlignment(Qt::AlignLeft);
    maxtext->setFont(QFont(font().family(), 12));
    maxtext->setPadding(QMargins(5, 5, 5, 5));
    maxtext->setBrush 	(QBrush(Qt::lightGray));

    mintext->setPositionAlignment(Qt::AlignLeft|Qt::AlignBottom);
    mintext->position->setCoords(-1, 0.5);
    mintext->setText("mi");
    mintext->setTextAlignment(Qt::AlignLeft);
    mintext->setFont(QFont(font().family(), 12));
    mintext->setPadding(QMargins(5, 5, 5, 5));
    mintext->setBrush 	(QBrush(Qt::lightGray));

    pptext->setPositionAlignment(Qt::AlignLeft|Qt::AlignVCenter);
    pptext->position->setCoords(-1, 0.5);
    pptext->setText("pp");
    pptext->setTextAlignment(Qt::AlignLeft);
    pptext->setFont(QFont(font().family(), 12));
    pptext->setPadding(QMargins(5, 5, 5, 5));
    pptext->setBrush 	(QBrush(Qt::lightGray));

    ui->cbsubsystem->addItem(QString("other"));
    ui->cbsubsystem->addItem(QString("Cs magnetometer"));
    ui->cbsubsystem->addItem(QString("Hg magnetometer"));
    ui->cbsubsystem->addItem(QString("Test measurement"));
    ui->cbsubsystem->addItem(QString("High voltage system"));
    ui->cbsubsystem->addItem(QString("Neutron guides"));
    ui->cbsubsystem->addItem(QString("Vacuum system"));
    ui->cbsubsystem->addItem(QString("RF coils"));
    ui->cbsubsystem->addItem(QString("Other parts inside vacuum tank"));
    ui->cbsubsystem->addItem(QString("B0, gradient, and trimcoils"));
    ui->cbsubsystem->addItem(QString(""));

    ui->box_rep->setTabOrder(ui->box_one, ui-> box_two);
    ui->box_rep->setTabOrder(ui-> box_two, ui-> box_rep);
    ui->box_rep->setTabOrder(ui-> box_rep, ui-> box_speed);
    //ui->box_rep->setTabOrder(ui-> box_speed, ui->filename);
    //ui->box_rep->setTabOrder(ui->filename, ui->go);
    ui->go->setTabOrder(ui->clear, ui->Browse);


    ui->label_status->setStyleSheet("background-color: red");

    //int first, second, loops;
    pos1 = -110;
    pos2 = -60;
    startpos = -200;
    loops = 2;
    movestate = idle;
    double speed;
    speed = 0.5;
    /*
    //QDir::setCurrent
    ui->box_one->setMinimum(-10);
    ui->box_one->setMaximum(50);
    ui->box_two->setMinimum(-10);
    ui->box_two->setMaximum(50);
    ui->box_speed->setMinimum(0.1);
    ui->box_speed->setMaximum(3.0);
    ui->box_one->setValue(first);
    ui->box_two->setValue(second);
    ui->box_rep->setValue(loops);
    ui->box_speed->setValue(speed);
    */

    ui->box_one->setMinimum(-90*4);
    ui->box_one->setMaximum(90*4);
    ui->box_two->setMinimum(-90*4);
    ui->box_two->setMaximum(90*4);

    ui->box_speed->setMinimum(0.1);
    ui->box_speed->setMaximum(3.0);

    ui->sb_startspeed->setMinimum(0.1);
    ui->sb_startspeed->setMaximum(10.0);
    ui->sb_startspeed->setValue(8.);

    ui->sb_acc->setMinimum(0.5);
    ui->sb_acc->setMaximum(5);
    ui->sb_acc->setValue(1.);


    ui->startpos->setMinimum(-90*4);
    ui->startpos->setMaximum(90*4);
    ui->startpos->setValue(startpos);

    ui->box_one->setValue(pos1);
    ui->box_two->setValue(pos2);
    ui->box_rep->setValue(loops);
    ui->box_speed->setValue(speed);

    ui->statusbar->showMessage("Status");

    ui->saveData->setChecked(true);

    ui->fnsate->setStyleSheet("background-color: rgba(255,100,100,255)");

    movestate=idle;
    doplot=false;
    graphcounter=0;
    data_matrix_ix=0;

    SetRunDirectory(false);
    ui->pbsave->setEnabled(false);
    metadatasaved=true;
}


MainWindow::~MainWindow()
{
    delete ui;
    delete timer;
}



void MainWindow::periodictimer()
{

    IPC_read_fbd(pwd);
/*
    if (pwd->fbd.samplecounter > 1)
    {
        ix=samplecounter%RINGBUFFERSIZE;
        currentsample=&pwd->map->samplebuffer[pwd->fbd.samplecounter-1];
        xval=currentsample->values[0]/SERVODISTANCESCALE;
        yval_1  = currentsample->values[1];
        yval_2  = currentsample->values[2];
        diff  = (yval_1 - yval_2)* 11.6/3.5*1000;
    }
*/
    ui->statusbar->showMessage(QString("volt2: %1 volt4:%2;  x:%3, y1: %4, y2:%5;  %6")
                               .arg(pwd->fbd.correction_voltage_coil2)
                               .arg(pwd->fbd.correction_voltage_coil4)
                               .arg(xval)
                               .arg(yval_1)
                               .arg(yval_2)
                               .arg(pwd->fbd.samplecounter)
                               );


    ui->lgoal->setText(QString("Goal Position: %1").arg(pwd->fbd.goalposition /SERVODISTANCESCALE,7,'f',3,' '));
    ui->lmpos->setText(QString("Motor Position: %1").arg(pwd->fbd.motorposition/SERVODISTANCESCALE,7,'f',3,' '));
    ui->lspeed->setText(QString("Speed: %1").arg(pwd->fbd.velocity/1000000.,7,'f',3));

/*
    for (unsigned long int i=0; i<sizeof(kp_spinBoxes)/sizeof(*kp_spinBoxes); i++ ) {
        kp_labels[i]->setText(QString("%1").arg(pwd->cfd.kp_p[i]));
        ki_labels[i]->setText(QString("%1").arg(pwd->cfd.ki_p[i]));

    }
*/
    double posdeviation = fabs((double) pwd->cfd.goalposition - pwd->fbd.motorposition)/SERVODISTANCESCALE;
    //bool movement_direction = pwd->cfd.goalposition > pwd->fbd.motorposition;
    pwd->cfd.tacc = accelerationtime;

    switch (movestate)
    {
    case idle:
        doplot=false;
        break;

    case setgoal1initial:
        clearData();
        data_matrix.clear();
        times.clear();
        data_matrix_ix=0;


        firstpoint=true;

        goal = pos1*SERVODISTANCESCALE;
        pwd->cfd.goalposition = goal;
        pwd->cfd.maxvel = startspeed*1000000.;
        pwd->cfd.tacc = accelerationtime;
        IPC_write_cfd(pwd);

        ui->Plot->xAxis->setRange(pos1-2,pos2+8);
        ui->Plot->replot();

        movestate = onthemove1initial;
        break;

    case onthemove1initial:
        ui->label_status->setText("Move to 1st position");
        if(posdeviation <0.005 &&  pwd->fbd.movestate == 0)
        {
            doplot=false;
            movestate = setgoal;
        }
        break;

    case setgoal:

        if (rep > graphcounter) // we have more repetitions to do
        {
            if (graphcounter % 2 == 0) // determine next goal
            {
                goal = pos2*SERVODISTANCESCALE;
            } else {
                goal = pos1*SERVODISTANCESCALE;
            }
            pwd->cfd.goalposition = goal;
            pwd->cfd.maxvel = measspeed*1000000.;
            IPC_write_cfd(pwd);

            xvalues.clear();
            yvalues.clear();
            graphcounter++;
            ui->Plot->addGraph();
            doplot=true;
            samplecounter = pwd->fbd.samplecounter;
            AppendFile();
            printf("goto move\n");

            movestate = onthemovemeasure;
        } else {
            movestate = setstart;
        }
        break;

    case onthemovemeasure:
        ui->label_status->setText(QString("recording graph %1/%2").arg(graphcounter).arg(rep));
        if(posdeviation < 0.005 &&  pwd->fbd.movestate == 0)
        {
            movestate = setgoal;
            doplot=false;
        }
        break;



    case setstart:
        doplot=false;

        startpos = ui->startpos->value();
        startspeed = ui->sb_startspeed->value();
        accelerationtime = ui->sb_acc->value();

        goal = (double)startpos*SERVODISTANCESCALE;
        //qDebug() <<"set start: " << goal<< endl;

        pwd->cfd.maxvel = (double) startspeed*1000000.;
        pwd->cfd.goalposition = goal;
        pwd->cfd.tacc = accelerationtime;

        IPC_write_cfd(pwd);
        movestate = movetostart;
        break;

    case movetostart:


        ui->label_status->setText("Move to start position");
        if(posdeviation <0.005 &&  pwd->fbd.movestate == 0)
        {
            doplot=false;
            movestate = finish;
        }
        break;

    case abort:

        /*
        // Work in progress, tryign to make the trolley stop earlier
        if move_direction{ // move to the right
            goal = pos2*SERVODISTANCESCALE;
        } else {    // move to the left
            goal = pos1*SERVODISTANCESCALE;
        }
        pwd->cfd.goalposition = goal;
        IPC_write_cfd(pwd);
        */

        movestate = abortdecelerate;
        break;

    case abortdecelerate:
        if(posdeviation <0.005 &&  pwd->fbd.movestate == 0)
        {
            // Remove data
            average_x.clear();
            average_single.clear();
            xvalues.clear();
            yvalues.clear();

            /*
             data_matrix.clear();
            times.clear();
            data_matrix_ix=0;
            */
            doplot=false;
            movestate = idle;

        }
        break;


    case finish:
        AppendFile();
        CloseFile();

        stop_time = QDateTime::currentDateTime();
        stop_time_string = stop_time.toString("yyyy-MM-dd_hh-mm-ss");

        average_x.clear();
        average_single.clear();
        xvalues.clear();
        yvalues.clear();

        //data_matrix.clear();
        //mes.clear();
        //ta_matrix_ix=0;

        movestate = idle;

        if (ui->doOnlineAnalysisCheck->isChecked()){
            onlineAnalysis();
        }

        break;
    }


    if (movestate != idle )
    {
        ui->emergency_stop_button->setEnabled(true);
        ui->go->setEnabled(false);
        ui->pbmvstart->setEnabled(false);
        ui->label_status->setStyleSheet("background-color: rgba(255,100,100,255)");


    } else {
        ui->emergency_stop_button->setEnabled(false);
        ui->go->setEnabled(true);
        ui->pbmvstart->setEnabled(true);
        ui->label_status->setText("Ready!");
        ui->label_status->setStyleSheet("background-color: rgba(100,255,100,255)");
    }



    if (pwd->fbd.samplecounter > 0 && doplot && graphcounter>0)
    {
        while (pwd->fbd.samplecounter > samplecounter)
        {
            ix=samplecounter%RINGBUFFERSIZE;
            currentsample=&pwd->map->samplebuffer[ix];
            yval_1  = currentsample->values[1]/CALIBRATIONCONSTANT*1000.;
            yval_2  = currentsample->values[2]/CALIBRATIONCONSTANT*1000.;
            diff  = yval_1 - yval_2;


            if(graphcounter % 2 == 0)
            {
                xval=currentsample->values[0]/SERVODISTANCESCALE;

            } else {
                xval=currentsample->values[0]/SERVODISTANCESCALE-0.8*ui->box_speed->value();
            }

            if (firstpoint)
            {
              starting_value=diff;
              firstpoint=false;
              ymin=0;
              ymax=0;
              xmin=xval;
              xmax=xval;
            }

            if (xval > xmax)
            {
                xmax = xval;
            }
            if (xval < xmin)
            {
                xmin = xval;
            }


            if (diff-starting_value > ymax)
            {
                ymax = diff-starting_value;
                maxline->point1->setCoords(1,ymax);
                maxline->point2->setCoords(2,ymax);

            }

            if (diff-starting_value < ymin)
            {
                ymin = diff-starting_value;
                minline->point1->setCoords(1,ymin);
                minline->point2->setCoords(2,ymin);

            }

            mintext->position->setCoords(pos2+1, ymin);
            mintext->setText(QString("%1").arg(ymin,0,'f',1,' '));

            maxtext->position->setCoords(pos2+1, ymax);
            maxtext->setText(QString("%1").arg(ymax,0,'f',1,' '));

            pptext->position->setCoords(pos2+1, (ymin+ymax)/2.);
            pptext->setText(QString("%1").arg(ymax-ymin,0,'f',1,' '));

            xvalues.push_back(xval);
            yvalues.push_back(diff-starting_value);

            data.push_back(graphcounter);
            data.push_back(xval);
            data.push_back(yval_1);
            data.push_back(yval_2);
            data.push_back(diff);

            data_matrix.push_back(data);
            data.clear();

            current_time = QDateTime::currentMSecsSinceEpoch();
            QString current_time_string = QString:: number(current_time) ;
            times.push_back(current_time_string) ;

            samplecounter++;
        }

        if (graphcounter % 2 == 0)
        {
            QPen pen;
            pen.setColor(Qt::red);
            ui->Plot->graph(graphcounter-1)->setPen(pen);
        }

        ui->Plot->graph(graphcounter-1)->setData(xvalues,yvalues);
//        ui->Plot->rescaleAxes(true);
//        ui->Plot->xAxis->scaleRange(1.2);
        ui->Plot->yAxis->setRange(ymin,ymax);
        ui->Plot->yAxis->scaleRange(1.1);

        ui->Plot->replot();
        ui->Plot->update();

    }

    ui->fnsate->setText(
                QStringLiteral("%1").arg(data_matrix_ix, 6, 10, QLatin1Char('0'))
                +" / "
                +QStringLiteral("%1").arg(data_matrix.size(), 6, 10, QLatin1Char('0'))
                );
            IPC_write_cfd(pwd);
}

void MainWindow::clearData()
{
    xvalues.clear();
    yvalues.clear();
    ui->Plot->clearGraphs();
    graphcounter=0;

    ui->Plot->update();
    ui->Plot->rescaleAxes(false);
    ui->Plot->replot();
}

bool MainWindow::InitFile()
{
    QString path=ui->directory->text();

    do
    {
        ui->measnumber->setValue(ui->measnumber->value()+1);
        file_name = path + '/'
                + QStringLiteral("%1").arg(ui->sbrunnumber->value(), 7, 10, QLatin1Char('0'))
                + "_"
                + QStringLiteral("%1").arg(ui->measnumber->value(), 4, 10, QLatin1Char('0'))
                + ".txt";
        outputfile.setFileName(file_name);
    }
    while(outputfile.exists());

    if (!outputfile.open(QIODevice::WriteOnly))
    {
        QMessageBox message;
        message.setText("Warning, file could not be opened.\nPlease check the filename and directory.");
        message.exec();
        return(false);
    } else {
        QTextStream stream(&outputfile);
        ui->filename->setText(file_name);
        ui->fnsate->setStyleSheet("background-color: rgba(255,255,000,255)");
        stream<< "Measurement,"<< "Timestamp,"<< "Position 2,"<< "Field Cell 2,"<< "Field Cell 4,"<< "Field diff"<<endl;
        ui->metadatafn->setText(path + '/'
                + QStringLiteral("%1").arg(ui->sbrunnumber->value(), 7, 10, QLatin1Char('0'))
                + "_"
                + QStringLiteral("%1").arg(ui->measnumber->value(), 4, 10, QLatin1Char('0'))
                + ".json");

        ui->mfnstate ->setStyleSheet("background-color: rgba(255,00,00,255)");
        ui->mfnstate->setText("not saved");
        metadatasaved=false;
        ui->pbsave->setEnabled(true);

        return(true);
    }

}

void MainWindow::AppendFile()
{
    printf("append: %ld, %d ",data_matrix_ix,data_matrix.size());

    QTextStream stream(&outputfile);
    printf("append: %ld, %d ",data_matrix_ix,data_matrix.size());
    while(data_matrix_ix < data_matrix.size())
    {
        stream << QString::number(data_matrix[data_matrix_ix][0])<<","<< times[data_matrix_ix]<<","<< QString::number(data_matrix[data_matrix_ix][1])<<","<<QString::number(data_matrix[data_matrix_ix][2])<<","<<QString::number(data_matrix[data_matrix_ix][3])<<","<<QString::number(data_matrix[data_matrix_ix][4])<<endl;
        data_matrix_ix++;
    }
    outputfile.flush();

    if(data_matrix_ix>0)
    {
        ui->fnsate->setStyleSheet("background-color: rgba(150,255,150,255)");
    }

    printf(",%ld\n",data_matrix_ix);
}


void MainWindow::CloseFile()
{
    outputfile.close();
    outputfile.setPermissions(QFileDevice::ReadOwner|QFileDevice::ReadGroup|QFileDevice::ReadOther);
    ui->fnsate->setStyleSheet("background-color: rgba(0,255,0,255)");

    if (ui->cbbackground->isChecked())
    {
      ui->fnbackground->setText(ui->filename->text());
    }
}

void MainWindow::onlineAnalysis()
{
    QString command = onlineAnalysisFilename;
    command += " " + file_name;
    if (ui->doBackgroundSubtractionCheck->isChecked()){
        command += " " + backgroundFilename;
    }
    std::cout << command.toLocal8Bit().data();
    std::system(command.toLocal8Bit().data());
}


void MainWindow::Average()
{

    doplot = false;
    /*
    int increments = 200; //number of steps

    float box_one = ui->box_one->value()*4;
    float box_two = ui->box_two->value()*4;

    float position_one = min(box_one, box_two);
    float position_two = max(box_one, box_two);


    int step_size = (position_two-position_one)/increments; // calculate how large the steps between the two end positions must be if it is divided into segments with equal width

    for (int step =0; step < increments ;step++ ) { // divide x range in 200 parts

        xsum = 0;
        ysum = 0;
        count = 0;

        for (int rows =0 ;rows < data_matrix.length() ; rows++ ) { //loop over all rows of data_matrix

            if((fabs(position_one) +fabs(step*step_size)< fabs(data_matrix[rows][0]) && fabs(data_matrix[rows][0]) < (fabs(position_one) +fabs((step+1)*step_size)))){

                xsum += data_matrix[rows][0];  //sum the x and y valuegrees
                ysum += data_matrix[rows][2];
                count ++;

            }

        }


            xsum = xsum/count; //calculate average
            ysum = ysum/count;

            //pairs.push_back(loop);
            mean_x.push_back(xsum); //create vector with x and y pair for every fragment
            mean_y.push_back(ysum);


         }

    QString new_file = "mean.txt";
    QFile file(new_file);
    file.open(QIODevice::WriteOnly);

    QTextStream stream(&file);

    stream<< "Mean y" << "mean_y"<<endl;

    for (int rows = 0; rows <mean_x.size()  ; rows++ ){
      stream << QString::number(mean_y[rows])<<","<< QString::number(mean_y[rows])<<endl;
      }


    ui->Plot->clearGraphs();
    ui->Plot->addGraph();
    ui->Plot->graph(0)->setData(mean_x, mean_y);
    ui->Plot->rescaleAxes(true);
    ui->Plot->replot();
    ui->Plot->update();



    */
}


void MainWindow::on_clear_clicked()
{
    clearData();
}


void MainWindow::on_go_clicked()
{
    if (!metadatasaved)
    {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(this, "Warning", "Metadata not saved. Save now?",
                                      QMessageBox::Yes|QMessageBox::No);
        if (reply == QMessageBox::Yes)
        {
            on_pbsave_clicked();
        }
    }

    ui->Plot->clearGraphs();
    ui->Plot->replot();
    doplot = true;
    ui->label_status->setText("Measurement is running");
    ui->label_status->setStyleSheet("background-color: green");

    start_time = QDateTime::currentDateTime();
    start_time_string = start_time.toString("yyyy-MM-dd_hh-mm-ss");

    if(InitFile())
    {
        //  startpos = ui->startpos->value();

        rep = ui->box_rep->value();
        pos1 = ui->box_one->value();
        pos2 = ui->box_two->value();

        startpos = ui->startpos->value();
        startspeed = ui->sb_startspeed->value();
        accelerationtime = ui->sb_acc->value();

        measspeed = ui->box_speed->value();

        movestate = setgoal1initial;
    }


}


void MainWindow::on_Browse_clicked()
{
    /*
    path = QFileDialog::getExistingDirectory(this, "Choose directory",path);
    //path =QFileDialog::getExistingDirectory(this, "Choose directory", QDir::homePath());
    //QDir::setCurrent(path);
    ui->label_directory->setText(path);
    */
}




void MainWindow::on_emergency_stop_button_clicked()
{
    movestate = abort;
}


void MainWindow::on_pbmvstart_clicked()
{
     movestate = setstart;
}


void MainWindow::on_chooseBackgroundFileButton_clicked()
{
    /*
    backgroundFilename = QFileDialog::getOpenFileName(this, "Choose directory",path);
//    ui->label_background->setText("Background: " + backgroundFilename);
*/
}


void MainWindow::on_chooseBackgroundFileButton_2_clicked()
{

}

bool MainWindow::SetRunDirectory(bool increase)
{
    QString pathtocreate;
    QDir dir;
    long runnumber=ui->sbrunnumber->value();

    if (increase)
    {
        do
        {
            runnumber=ui->sbrunnumber->value()+1;
            ui->sbrunnumber->setValue(runnumber);
            long runthousands=runnumber/1000;
            pathtocreate="/n2edm/datafiles/"+ QStringLiteral("%1").arg(runthousands, 4, 10, QLatin1Char('0'))+"/"+QStringLiteral("%1").arg(runnumber, 7, 10, QLatin1Char('0'));
            dir.setPath(pathtocreate);
        }
        while(dir.exists());
    } else  {
        long runthousands=runnumber/1000;
        pathtocreate="/n2edm/datafiles/"+ QStringLiteral("%1").arg(runthousands, 4, 10, QLatin1Char('0'))+"/"+QStringLiteral("%1").arg(runnumber, 7, 10, QLatin1Char('0'));
        dir.setPath(pathtocreate);
    }

    if (dir.exists())
    {
       ui->directory->setText(pathtocreate);
       pwd->cfd.currentrun=runnumber;
    }
    else
    {
      if (dir.mkpath(pathtocreate))
      {
          ui->directory->setText(pathtocreate);
          pwd->cfd.currentrun=runnumber;
          ui->measnumber->setValue(0);
      }
    }

}

void MainWindow::on_NewRunButton_clicked()
{
    SetRunDirectory(true);
    ui->fnbackground->setText("none");
}

void MainWindow::on_pbsave_clicked()
{
    QFile metafile;
    metafile.setFileName(ui->metadatafn->text());
    if (metafile.exists())
    {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(this, "Metadata file exists", "Overwrite file?",
                                        QMessageBox::Yes|QMessageBox::No);
          if (reply == QMessageBox::No) {
            return;
          }
    }

    if (!metafile.open(QIODevice::WriteOnly))
    {
        QMessageBox message;
        message.setText("Warning, file could not be opened.\nPlease check the filename and directory.");
        message.exec();
        return;
    }

    metafile.resize(0);

    QJsonObject jo;

         jo.insert("run", ui->sbrunnumber->value());
         jo.insert("measurment",ui->measnumber->value());
         jo.insert("datafile",ui->filename->text());
         jo.insert("metafile",ui->metadatafn->text());
         jo.insert("starttime",start_time_string);
         jo.insert("stoptime",stop_time_string);
         jo.insert("repetitions",ui->box_rep->value());
         jo.insert("subsystem",ui->cbsubsystem->currentText());
         jo.insert("part",ui->lepart->text());
         jo.insert("magnetized",ui->cbmagnetized->isChecked());
         jo.insert("backgroundmeasurement",ui->cbbackground->isChecked());
         jo.insert("comment",ui->lecomment->text());
         jo.insert("lastbackground",ui->fnbackground->text());
         jo.insert("startposition",ui->startpos->value());
         jo.insert("measposition1",ui->box_one->value());
         jo.insert("measposition2",ui->box_two->value());
         jo.insert("measspeed",ui->box_speed->value());
         jo.insert("startpositionspeed",ui->sb_startspeed->value());
         jo.insert("accelerationtime",ui->sb_acc->value());
         jo.insert("calibrationconstant",CALIBRATIONCONSTANT);
         jo.insert("ymin",ymin);
         jo.insert("ymax",ymax);

        QJsonDocument doc;
        doc.setObject(jo);
        if (metafile.write(doc.toJson())<=0)
        {
            QMessageBox message;
            message.setText("Warning, file could not be writen.\nPlease check the filename and directory.");
            message.exec();
            return;
        } else
        {
            ui->mfnstate ->setStyleSheet("background-color: rgba(0,255,00,255)");
            ui->mfnstate->setText("saved");
            metadatasaved=true;
        }
        metafile.close();
}
