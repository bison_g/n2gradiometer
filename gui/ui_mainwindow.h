/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout;
    QFrame *frame;
    QHBoxLayout *horizontalLayout;
    QPushButton *pbmvstart;
    QPushButton *emergency_stop_button;
    QLabel *lgoal;
    QLabel *lmpos;
    QLabel *lspeed;
    QLabel *label_status;
    QPushButton *go;
    QGridLayout *gridLayout_3;
    QCustomPlot *Plot;
    QTabWidget *tabWidget;
    QWidget *tab_GradCtrl;
    QGridLayout *gridLayout;
    QPushButton *NewRunButton;
    QCheckBox *cbmagnetized;
    QLabel *label_15;
    QLabel *label_8;
    QComboBox *cbsubsystem;
    QLabel *label_12;
    QSpinBox *sbrunnumber;
    QLabel *label_14;
    QLabel *label_3;
    QSpinBox *box_rep;
    QLineEdit *lecomment;
    QSpinBox *measnumber;
    QCheckBox *saveData;
    QLabel *label_7;
    QLabel *directory;
    QLabel *filename;
    QLabel *label_10;
    QPushButton *clear;
    QLabel *metadatafn;
    QLabel *label_13;
    QLineEdit *lepart;
    QLabel *label_11;
    QLabel *fnsate;
    QLabel *mfnstate;
    QPushButton *pbsave;
    QCheckBox *cbbackground;
    QLabel *label_16;
    QLabel *fnbackground;
    QWidget *tab_Parameters;
    QGridLayout *gridLayout_2;
    QLabel *label;
    QSpinBox *box_one;
    QSpacerItem *verticalSpacer;
    QDoubleSpinBox *startpos;
    QLabel *label_6;
    QLabel *label_5;
    QDoubleSpinBox *sb_acc;
    QSpinBox *box_two;
    QDoubleSpinBox *box_speed;
    QDoubleSpinBox *sb_startspeed;
    QLabel *label_2;
    QLabel *label_4;
    QLabel *label_9;
    QWidget *tab;
    QPushButton *chooseBackgroundFileButton;
    QPushButton *Browse;
    QCheckBox *doOnlineAnalysisCheck;
    QCheckBox *doBackgroundSubtractionCheck;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1391, 948);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayout = new QVBoxLayout(centralwidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        frame = new QFrame(centralwidget);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setMinimumSize(QSize(0, 60));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        pbmvstart = new QPushButton(frame);
        pbmvstart->setObjectName(QString::fromUtf8("pbmvstart"));
        pbmvstart->setMaximumSize(QSize(150, 16777215));

        horizontalLayout->addWidget(pbmvstart);

        emergency_stop_button = new QPushButton(frame);
        emergency_stop_button->setObjectName(QString::fromUtf8("emergency_stop_button"));
        emergency_stop_button->setEnabled(true);
        emergency_stop_button->setMaximumSize(QSize(150, 16777215));

        horizontalLayout->addWidget(emergency_stop_button);

        lgoal = new QLabel(frame);
        lgoal->setObjectName(QString::fromUtf8("lgoal"));
        lgoal->setMinimumSize(QSize(150, 0));
        lgoal->setMaximumSize(QSize(150, 16777215));

        horizontalLayout->addWidget(lgoal);

        lmpos = new QLabel(frame);
        lmpos->setObjectName(QString::fromUtf8("lmpos"));
        lmpos->setMinimumSize(QSize(200, 0));
        lmpos->setMaximumSize(QSize(150, 16777215));

        horizontalLayout->addWidget(lmpos);

        lspeed = new QLabel(frame);
        lspeed->setObjectName(QString::fromUtf8("lspeed"));
        lspeed->setMaximumSize(QSize(150, 16777215));

        horizontalLayout->addWidget(lspeed);

        label_status = new QLabel(frame);
        label_status->setObjectName(QString::fromUtf8("label_status"));
        label_status->setEnabled(true);
        label_status->setMinimumSize(QSize(50, 0));
        label_status->setFrameShape(QFrame::StyledPanel);
        label_status->setLineWidth(3);
        label_status->setIndent(-3);

        horizontalLayout->addWidget(label_status);

        go = new QPushButton(frame);
        go->setObjectName(QString::fromUtf8("go"));
        go->setEnabled(true);
        go->setAutoDefault(true);

        horizontalLayout->addWidget(go);


        verticalLayout->addWidget(frame);

        gridLayout_3 = new QGridLayout();
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        Plot = new QCustomPlot(centralwidget);
        Plot->setObjectName(QString::fromUtf8("Plot"));

        gridLayout_3->addWidget(Plot, 1, 0, 1, 1);

        tabWidget = new QTabWidget(centralwidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setMaximumSize(QSize(16777215, 300));
        tab_GradCtrl = new QWidget();
        tab_GradCtrl->setObjectName(QString::fromUtf8("tab_GradCtrl"));
        gridLayout = new QGridLayout(tab_GradCtrl);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        NewRunButton = new QPushButton(tab_GradCtrl);
        NewRunButton->setObjectName(QString::fromUtf8("NewRunButton"));
        NewRunButton->setMaximumSize(QSize(120, 16777215));

        gridLayout->addWidget(NewRunButton, 1, 3, 1, 1);

        cbmagnetized = new QCheckBox(tab_GradCtrl);
        cbmagnetized->setObjectName(QString::fromUtf8("cbmagnetized"));

        gridLayout->addWidget(cbmagnetized, 7, 2, 1, 1);

        label_15 = new QLabel(tab_GradCtrl);
        label_15->setObjectName(QString::fromUtf8("label_15"));

        gridLayout->addWidget(label_15, 2, 4, 1, 1);

        label_8 = new QLabel(tab_GradCtrl);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        gridLayout->addWidget(label_8, 3, 0, 1, 1);

        cbsubsystem = new QComboBox(tab_GradCtrl);
        cbsubsystem->setObjectName(QString::fromUtf8("cbsubsystem"));
        cbsubsystem->setEditable(true);

        gridLayout->addWidget(cbsubsystem, 3, 2, 1, 2);

        label_12 = new QLabel(tab_GradCtrl);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        gridLayout->addWidget(label_12, 2, 0, 1, 1);

        sbrunnumber = new QSpinBox(tab_GradCtrl);
        sbrunnumber->setObjectName(QString::fromUtf8("sbrunnumber"));
        sbrunnumber->setMaximumSize(QSize(200, 16777215));
        sbrunnumber->setMaximum(5000);

        gridLayout->addWidget(sbrunnumber, 1, 2, 1, 1);

        label_14 = new QLabel(tab_GradCtrl);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        gridLayout->addWidget(label_14, 3, 4, 1, 1);

        label_3 = new QLabel(tab_GradCtrl);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout->addWidget(label_3, 10, 0, 1, 1);

        box_rep = new QSpinBox(tab_GradCtrl);
        box_rep->setObjectName(QString::fromUtf8("box_rep"));
        box_rep->setMaximum(5000);

        gridLayout->addWidget(box_rep, 10, 2, 1, 2);

        lecomment = new QLineEdit(tab_GradCtrl);
        lecomment->setObjectName(QString::fromUtf8("lecomment"));

        gridLayout->addWidget(lecomment, 8, 2, 2, 2);

        measnumber = new QSpinBox(tab_GradCtrl);
        measnumber->setObjectName(QString::fromUtf8("measnumber"));

        gridLayout->addWidget(measnumber, 2, 2, 1, 1);

        saveData = new QCheckBox(tab_GradCtrl);
        saveData->setObjectName(QString::fromUtf8("saveData"));

        gridLayout->addWidget(saveData, 2, 3, 1, 1);

        label_7 = new QLabel(tab_GradCtrl);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setMaximumSize(QSize(150, 16777215));

        gridLayout->addWidget(label_7, 1, 0, 1, 1);

        directory = new QLabel(tab_GradCtrl);
        directory->setObjectName(QString::fromUtf8("directory"));

        gridLayout->addWidget(directory, 1, 5, 1, 1);

        filename = new QLabel(tab_GradCtrl);
        filename->setObjectName(QString::fromUtf8("filename"));

        gridLayout->addWidget(filename, 2, 5, 1, 1);

        label_10 = new QLabel(tab_GradCtrl);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        gridLayout->addWidget(label_10, 4, 0, 2, 1);

        clear = new QPushButton(tab_GradCtrl);
        clear->setObjectName(QString::fromUtf8("clear"));

        gridLayout->addWidget(clear, 10, 4, 1, 1);

        metadatafn = new QLabel(tab_GradCtrl);
        metadatafn->setObjectName(QString::fromUtf8("metadatafn"));

        gridLayout->addWidget(metadatafn, 3, 5, 1, 1);

        label_13 = new QLabel(tab_GradCtrl);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setMaximumSize(QSize(150, 16777215));

        gridLayout->addWidget(label_13, 1, 4, 1, 1);

        lepart = new QLineEdit(tab_GradCtrl);
        lepart->setObjectName(QString::fromUtf8("lepart"));

        gridLayout->addWidget(lepart, 4, 2, 1, 2);

        label_11 = new QLabel(tab_GradCtrl);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        gridLayout->addWidget(label_11, 8, 0, 2, 1);

        fnsate = new QLabel(tab_GradCtrl);
        fnsate->setObjectName(QString::fromUtf8("fnsate"));
        fnsate->setMaximumSize(QSize(110, 16777215));
        fnsate->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(fnsate, 2, 6, 1, 1);

        mfnstate = new QLabel(tab_GradCtrl);
        mfnstate->setObjectName(QString::fromUtf8("mfnstate"));
        mfnstate->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(mfnstate, 3, 6, 1, 1);

        pbsave = new QPushButton(tab_GradCtrl);
        pbsave->setObjectName(QString::fromUtf8("pbsave"));

        gridLayout->addWidget(pbsave, 4, 6, 1, 1);

        cbbackground = new QCheckBox(tab_GradCtrl);
        cbbackground->setObjectName(QString::fromUtf8("cbbackground"));

        gridLayout->addWidget(cbbackground, 7, 3, 1, 1);

        label_16 = new QLabel(tab_GradCtrl);
        label_16->setObjectName(QString::fromUtf8("label_16"));

        gridLayout->addWidget(label_16, 7, 4, 1, 1);

        fnbackground = new QLabel(tab_GradCtrl);
        fnbackground->setObjectName(QString::fromUtf8("fnbackground"));

        gridLayout->addWidget(fnbackground, 7, 5, 1, 1);

        tabWidget->addTab(tab_GradCtrl, QString());
        tab_Parameters = new QWidget();
        tab_Parameters->setObjectName(QString::fromUtf8("tab_Parameters"));
        gridLayout_2 = new QGridLayout(tab_Parameters);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        label = new QLabel(tab_Parameters);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout_2->addWidget(label, 1, 0, 1, 1);

        box_one = new QSpinBox(tab_Parameters);
        box_one->setObjectName(QString::fromUtf8("box_one"));
        box_one->setEnabled(true);
        box_one->setMinimumSize(QSize(0, 0));
        box_one->setMinimum(-15);
        box_one->setMaximum(50);
        box_one->setValue(0);
        box_one->setDisplayIntegerBase(10);

        gridLayout_2->addWidget(box_one, 1, 2, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_2->addItem(verticalSpacer, 4, 2, 1, 1);

        startpos = new QDoubleSpinBox(tab_Parameters);
        startpos->setObjectName(QString::fromUtf8("startpos"));

        gridLayout_2->addWidget(startpos, 0, 2, 1, 1);

        label_6 = new QLabel(tab_Parameters);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        gridLayout_2->addWidget(label_6, 1, 3, 1, 1);

        label_5 = new QLabel(tab_Parameters);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout_2->addWidget(label_5, 0, 0, 1, 1);

        sb_acc = new QDoubleSpinBox(tab_Parameters);
        sb_acc->setObjectName(QString::fromUtf8("sb_acc"));

        gridLayout_2->addWidget(sb_acc, 3, 4, 1, 1);

        box_two = new QSpinBox(tab_Parameters);
        box_two->setObjectName(QString::fromUtf8("box_two"));
        box_two->setMinimum(-15);
        box_two->setMaximum(50);

        gridLayout_2->addWidget(box_two, 3, 2, 1, 1);

        box_speed = new QDoubleSpinBox(tab_Parameters);
        box_speed->setObjectName(QString::fromUtf8("box_speed"));

        gridLayout_2->addWidget(box_speed, 0, 4, 1, 1);

        sb_startspeed = new QDoubleSpinBox(tab_Parameters);
        sb_startspeed->setObjectName(QString::fromUtf8("sb_startspeed"));

        gridLayout_2->addWidget(sb_startspeed, 1, 4, 1, 1);

        label_2 = new QLabel(tab_Parameters);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout_2->addWidget(label_2, 3, 0, 1, 1);

        label_4 = new QLabel(tab_Parameters);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout_2->addWidget(label_4, 0, 3, 1, 1);

        label_9 = new QLabel(tab_Parameters);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        gridLayout_2->addWidget(label_9, 3, 3, 1, 1);

        tabWidget->addTab(tab_Parameters, QString());
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        chooseBackgroundFileButton = new QPushButton(tab);
        chooseBackgroundFileButton->setObjectName(QString::fromUtf8("chooseBackgroundFileButton"));
        chooseBackgroundFileButton->setGeometry(QRect(390, 130, 286, 30));
        Browse = new QPushButton(tab);
        Browse->setObjectName(QString::fromUtf8("Browse"));
        Browse->setGeometry(QRect(270, 70, 286, 31));
        doOnlineAnalysisCheck = new QCheckBox(tab);
        doOnlineAnalysisCheck->setObjectName(QString::fromUtf8("doOnlineAnalysisCheck"));
        doOnlineAnalysisCheck->setGeometry(QRect(160, 130, 577, 22));
        doBackgroundSubtractionCheck = new QCheckBox(tab);
        doBackgroundSubtractionCheck->setObjectName(QString::fromUtf8("doBackgroundSubtractionCheck"));
        doBackgroundSubtractionCheck->setGeometry(QRect(160, 168, 577, 22));
        tabWidget->addTab(tab, QString());

        gridLayout_3->addWidget(tabWidget, 0, 0, 1, 1);

        gridLayout_3->setRowStretch(0, 1);

        verticalLayout->addLayout(gridLayout_3);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 1391, 30));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        go->setDefault(true);
        tabWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Gradiometer GUI", nullptr));
        pbmvstart->setText(QApplication::translate("MainWindow", "Move to start", nullptr));
        emergency_stop_button->setText(QApplication::translate("MainWindow", "Stop measurement", nullptr));
        lgoal->setText(QApplication::translate("MainWindow", "Goal Position:", nullptr));
        lmpos->setText(QApplication::translate("MainWindow", "Motor Position:", nullptr));
        lspeed->setText(QApplication::translate("MainWindow", "TextLabel", nullptr));
        label_status->setText(QApplication::translate("MainWindow", "Waiting to start", nullptr));
        go->setText(QApplication::translate("MainWindow", "Go", nullptr));
#ifndef QT_NO_SHORTCUT
        go->setShortcut(QApplication::translate("MainWindow", "Return", nullptr));
#endif // QT_NO_SHORTCUT
        NewRunButton->setText(QApplication::translate("MainWindow", "New Run", nullptr));
        cbmagnetized->setText(QApplication::translate("MainWindow", "Magnetized", nullptr));
        label_15->setText(QApplication::translate("MainWindow", "Current data filename", nullptr));
        label_8->setText(QApplication::translate("MainWindow", "Subsystem", nullptr));
        label_12->setText(QApplication::translate("MainWindow", "Measurment number", nullptr));
        label_14->setText(QApplication::translate("MainWindow", "Current metadata file", nullptr));
        label_3->setText(QApplication::translate("MainWindow", "Graph Repetitions", nullptr));
        saveData->setText(QApplication::translate("MainWindow", "Save data?", nullptr));
        label_7->setText(QApplication::translate("MainWindow", "Run number", nullptr));
        directory->setText(QApplication::translate("MainWindow", "Directory", nullptr));
        filename->setText(QString());
        label_10->setText(QApplication::translate("MainWindow", "Part", nullptr));
        clear->setText(QApplication::translate("MainWindow", "Clear", nullptr));
        metadatafn->setText(QString());
        label_13->setText(QApplication::translate("MainWindow", "Current directory", nullptr));
        label_11->setText(QApplication::translate("MainWindow", "Comment", nullptr));
        fnsate->setText(QApplication::translate("MainWindow", "TextLabel", nullptr));
        mfnstate->setText(QString());
        pbsave->setText(QApplication::translate("MainWindow", "Save Metadata", nullptr));
        cbbackground->setText(QApplication::translate("MainWindow", "Background", nullptr));
        label_16->setText(QApplication::translate("MainWindow", "Last background file", nullptr));
        fnbackground->setText(QApplication::translate("MainWindow", "none", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_GradCtrl), QApplication::translate("MainWindow", "Gradiometer Control", nullptr));
        label->setText(QApplication::translate("MainWindow", "1st meas. position (cm)", nullptr));
        label_6->setText(QApplication::translate("MainWindow", "Start position speed ", nullptr));
        label_5->setText(QApplication::translate("MainWindow", "Start Position (cm)", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "2nd meas position (cm)", nullptr));
        label_4->setText(QApplication::translate("MainWindow", "Measurement speed", nullptr));
        label_9->setText(QApplication::translate("MainWindow", "Acceleration time", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_Parameters), QApplication::translate("MainWindow", "Parameters", nullptr));
        chooseBackgroundFileButton->setText(QApplication::translate("MainWindow", "Choose background", nullptr));
        Browse->setText(QApplication::translate("MainWindow", "Choose save directory", nullptr));
        doOnlineAnalysisCheck->setText(QApplication::translate("MainWindow", "Execute", nullptr));
        doBackgroundSubtractionCheck->setText(QApplication::translate("MainWindow", "Subtract Background", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("MainWindow", "Page", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
