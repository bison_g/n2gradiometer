#include "firfilterlib.h"

#include "DSF10filters.h"
#include "DSF100filters.h"


#define FILTERLEN01 10

double filter01[FILTERLEN01] = { 1., 1., 1., 1., 1., 1., 1., 1., 1., 1. };


#define FILTERLEN02 5

double filter02[FILTERLEN02] = { 1., 1., 1., 1., 1.};



int FIRinit (fir_filter_status_t *fs, double *filter, long filterlen, long DSF)
{
  fs->filter = filter;

  fs->filterlen = filterlen;
  fs->inputbufferlen = fs->filterlen;
  fs->inputbufferpos = 0;
  fs->downsamplingfactor = DSF;
  fs->outputcount = 0;

  fs->inputbuffer = new double[fs->inputbufferlen];
  if  (fs->inputbuffer == NULL)
  {
    return -1;
  }

  return 0;
}



int FIRinit01 (fir_filter_status_t *fs)
{

  fs->filter = filter01;

  fs->filterlen = FILTERLEN01;
  fs->inputbufferlen = fs->filterlen;
  fs->inputbufferpos = 0;
  fs->downsamplingfactor = 1;
  fs->outputcount = 0;

  fs->inputbuffer = new double[fs->inputbufferlen];
  if  (fs->inputbuffer == NULL)
  {
    return -1;
  }

  return 0;

}

int FIRinit02 (fir_filter_status_t *fs)
{

  fs->filter = filter02;

  fs->filterlen = FILTERLEN02;
  fs->inputbufferlen = fs->filterlen;
  fs->inputbufferpos = 0;
  fs->downsamplingfactor = 1;
  fs->outputcount = 0;

  fs->inputbuffer = new double[fs->inputbufferlen];
  if  (fs->inputbuffer == NULL)
  {
    return -1;
  }

  return 0;

}

void FIRdelete (fir_filter_status_t *fs)
{
  delete fs->inputbuffer;
  fs->inputbuffer = NULL;
}



long FIRdownsample (double *inputvector, double *outputvector, fir_filter_status_t *fs)
{

  int64_t j, i;
//  int64_t inputcount;
  double sum;

  j = 0;

  fs->outputcount = 0;
  fs->inputcount = 0;

  if (fs->inputlen <= 0)
  {
    return 0;
  }



  while ( j <= fs->inputbufferpos + fs->inputlen - fs->filterlen
          &&
          fs->outputcount < fs->outputlen )
  {

    sum = 0;

    for (i = 0; i < fs->filterlen; i++)
    {
      if (j + i < fs->inputbufferpos )
      {
        sum += fs->inputbuffer[j + i] * fs->filter[i];

      }
      else
      {
        sum += inputvector[j + i - fs->inputbufferpos] * fs->filter[i];

      }
    }
    outputvector[fs->outputcount] = sum;


    j += fs->downsamplingfactor;
    fs->outputcount++;


  }

  i = 0;
  while (j < fs->inputbufferpos + fs->inputlen && i < fs->inputbufferlen)
  {

    if (j < fs->inputbufferpos )
    {
      fs->inputbuffer[i] = fs->inputbuffer[j];
    }
    else
    {
      fs->inputbuffer[i] = inputvector[j - fs->inputbufferpos];
    }

    i++;
    j++;
  }

  fs->inputcount = j - fs->inputbufferpos;
  fs->inputbufferpos = i;

return fs->outputcount;


} // FIRdownsample

int FIRinitMultistage (fir_filter_status_multistage_t *fs, int64_t bufferlen)
{
  fs->outputcount = 0;
  fs->bufferlen = bufferlen;
  fs->buffer[0] = new double[fs->bufferlen];
  fs->buffer[1] = new double[fs->bufferlen];
  fs->filterstages = 3;
  fs->statusvector = new fir_filter_status_t[fs->filterstages];

  FIRinit02(&fs->statusvector[0]);
  FIRinit02(&fs->statusvector[1]);
  FIRinit02(&fs->statusvector[2]);

  return 0;

}



int FIRinitMultistage_DSF10 (fir_filter_status_multistage_t *fs, int64_t bufferlen)
{
  fs->outputcount = 0;
  fs->bufferlen = bufferlen;
  fs->buffer[0] = new double[fs->bufferlen];
  fs->buffer[1] = new double[fs->bufferlen];
  fs->filterstages = 2;
  fs->statusvector = new fir_filter_status_t[fs->filterstages];

  FIRinit(&fs->statusvector[0], filter_DSF10_2_00, FILTERLEN_DSF10_2_00, 2);
  FIRinit(&fs->statusvector[1], filter_DSF10_5_01, FILTERLEN_DSF10_5_01, 5);

  return 0;

}

int FIRinitMultistage_DSF5000 (fir_filter_status_multistage_t *fs, int64_t bufferlen)
{
  fs->outputcount = 0;
  fs->bufferlen = bufferlen;
  fs->buffer[0] = new double[fs->bufferlen];
  fs->buffer[1] = new double[fs->bufferlen];
  fs->filterstages = 7;
  fs->statusvector = new fir_filter_status_t[fs->filterstages];

  FIRinit(&fs->statusvector[0], filter_DSF100_2_00, FILTERLEN_DSF100_2_00, 2);
  FIRinit(&fs->statusvector[1], filter_DSF100_2_00, FILTERLEN_DSF100_2_00, 2);
  FIRinit(&fs->statusvector[2], filter_DSF100_2_00, FILTERLEN_DSF100_2_00, 2);
  FIRinit(&fs->statusvector[3], filter_DSF100_5_01, FILTERLEN_DSF100_5_01, 5);
  FIRinit(&fs->statusvector[4], filter_DSF100_5_01, FILTERLEN_DSF100_5_01, 5);
  FIRinit(&fs->statusvector[5], filter_DSF100_5_01, FILTERLEN_DSF100_5_01, 5);
  FIRinit(&fs->statusvector[6], filter_DSF100_5_02, FILTERLEN_DSF100_5_02, 5);

  return 0;

}


int FIRinitMultistage_DSF2000 (fir_filter_status_multistage_t *fs, int64_t bufferlen)
{
  fs->outputcount = 0;
  fs->bufferlen = bufferlen;
  fs->buffer[0] = new double[fs->bufferlen];
  fs->buffer[1] = new double[fs->bufferlen];
  fs->filterstages = 7;
  fs->statusvector = new fir_filter_status_t[fs->filterstages];

  FIRinit(&fs->statusvector[0], filter_DSF100_2_00, FILTERLEN_DSF100_2_00, 2);
  FIRinit(&fs->statusvector[1], filter_DSF100_2_00, FILTERLEN_DSF100_2_00, 2);
  FIRinit(&fs->statusvector[2], filter_DSF100_2_00, FILTERLEN_DSF100_2_00, 2);
  FIRinit(&fs->statusvector[3], filter_DSF100_2_00, FILTERLEN_DSF100_2_00, 2);
  FIRinit(&fs->statusvector[4], filter_DSF100_5_01, FILTERLEN_DSF100_5_01, 5);
  FIRinit(&fs->statusvector[5], filter_DSF100_5_01, FILTERLEN_DSF100_5_01, 5);
  FIRinit(&fs->statusvector[6], filter_DSF100_5_02, FILTERLEN_DSF100_5_02, 5);

  return 0;

}


int FIRinitMultistage_DSF1000 (fir_filter_status_multistage_t *fs, int64_t bufferlen)
{
  fs->outputcount = 0;
  fs->bufferlen = bufferlen;
  fs->buffer[0] = new double[fs->bufferlen];
  fs->buffer[1] = new double[fs->bufferlen];
  fs->filterstages = 6;
  fs->statusvector = new fir_filter_status_t[fs->filterstages];

  FIRinit(&fs->statusvector[0], filter_DSF100_2_00, FILTERLEN_DSF100_2_00, 2);
  FIRinit(&fs->statusvector[1], filter_DSF100_2_00, FILTERLEN_DSF100_2_00, 2);
  FIRinit(&fs->statusvector[2], filter_DSF100_2_00, FILTERLEN_DSF100_2_00, 2);
  FIRinit(&fs->statusvector[3], filter_DSF100_5_01, FILTERLEN_DSF100_5_01, 5);
  FIRinit(&fs->statusvector[4], filter_DSF100_5_01, FILTERLEN_DSF100_5_01, 5);
  FIRinit(&fs->statusvector[5], filter_DSF100_5_02, FILTERLEN_DSF100_5_02, 5);

  return 0;

}


int FIRinitMultistage_DSF500 (fir_filter_status_multistage_t *fs, int64_t bufferlen)
{
  fs->outputcount = 0;
  fs->bufferlen = bufferlen;
  fs->buffer[0] = new double[fs->bufferlen];
  fs->buffer[1] = new double[fs->bufferlen];
  fs->filterstages = 5;
  fs->statusvector = new fir_filter_status_t[fs->filterstages];

  FIRinit(&fs->statusvector[0], filter_DSF100_2_00, FILTERLEN_DSF100_2_00, 2);
  FIRinit(&fs->statusvector[1], filter_DSF100_2_00, FILTERLEN_DSF100_2_00, 2);
  FIRinit(&fs->statusvector[2], filter_DSF100_5_01, FILTERLEN_DSF100_5_01, 5);
  FIRinit(&fs->statusvector[3], filter_DSF100_5_01, FILTERLEN_DSF100_5_01, 5);
  FIRinit(&fs->statusvector[4], filter_DSF100_5_02, FILTERLEN_DSF100_5_02, 5);

  return 0;

}


int FIRinitMultistage_DSF200 (fir_filter_status_multistage_t *fs, int64_t bufferlen)
{
  fs->outputcount = 0;
  fs->bufferlen = bufferlen;
  fs->buffer[0] = new double[fs->bufferlen];
  fs->buffer[1] = new double[fs->bufferlen];
  fs->filterstages = 5;
  fs->statusvector = new fir_filter_status_t[fs->filterstages];

  FIRinit(&fs->statusvector[0], filter_DSF100_2_00, FILTERLEN_DSF100_2_00, 2);
  FIRinit(&fs->statusvector[1], filter_DSF100_2_00, FILTERLEN_DSF100_2_00, 2);
  FIRinit(&fs->statusvector[2], filter_DSF100_2_00, FILTERLEN_DSF100_2_00, 2);
  FIRinit(&fs->statusvector[3], filter_DSF100_5_01, FILTERLEN_DSF100_5_01, 5);
  FIRinit(&fs->statusvector[4], filter_DSF100_5_02, FILTERLEN_DSF100_5_02, 5);

  return 0;

}

int FIRinitMultistage_DSF100 (fir_filter_status_multistage_t *fs, int64_t bufferlen)
{
  fs->outputcount = 0;
  fs->bufferlen = bufferlen;
  fs->buffer[0] = new double[fs->bufferlen];
  fs->buffer[1] = new double[fs->bufferlen];
  fs->filterstages = 4;
  fs->statusvector = new fir_filter_status_t[fs->filterstages];

  FIRinit(&fs->statusvector[0], filter_DSF100_2_00, FILTERLEN_DSF100_2_00, 2);
  FIRinit(&fs->statusvector[1], filter_DSF100_2_00, FILTERLEN_DSF100_2_00, 2);
  FIRinit(&fs->statusvector[2], filter_DSF100_5_01, FILTERLEN_DSF100_5_01, 5);
  FIRinit(&fs->statusvector[3], filter_DSF100_5_02, FILTERLEN_DSF100_5_02, 5);

  return 0;

}


int FIRinitMultistage_DSF50 (fir_filter_status_multistage_t *fs, int64_t bufferlen)
{
  fs->outputcount = 0;
  fs->bufferlen = bufferlen;
  fs->buffer[0] = new double[fs->bufferlen];
  fs->buffer[1] = new double[fs->bufferlen];
  fs->filterstages = 3;
  fs->statusvector = new fir_filter_status_t[fs->filterstages];

  FIRinit(&fs->statusvector[0], filter_DSF100_2_00, FILTERLEN_DSF100_2_00, 2);
  FIRinit(&fs->statusvector[1], filter_DSF100_5_01, FILTERLEN_DSF100_5_01, 5);
  FIRinit(&fs->statusvector[2], filter_DSF100_5_02, FILTERLEN_DSF100_5_02, 5);

  return 0;

}

int FIRinitMultistage_DSF25 (fir_filter_status_multistage_t *fs, int64_t bufferlen)
{
  fs->outputcount = 0;
  fs->bufferlen = bufferlen;
  fs->buffer[0] = new double[fs->bufferlen];
  fs->buffer[1] = new double[fs->bufferlen];
  fs->filterstages = 2;
  fs->statusvector = new fir_filter_status_t[fs->filterstages];

  FIRinit(&fs->statusvector[0], filter_DSF100_5_01, FILTERLEN_DSF100_5_01, 5);
  FIRinit(&fs->statusvector[1], filter_DSF100_5_02, FILTERLEN_DSF100_5_02, 5);

  return 0;

}


void FIRdeleteMultistage (fir_filter_status_multistage_t *fs)
{
  for (int i = 0; i < fs->filterstages; i++)
  {
    FIRdelete(&fs->statusvector[i]);
  }
  delete fs->buffer[0];
  delete fs->buffer[1];
  delete fs->statusvector;

}







long FIRdownsampleMultistage (double *inputvector, double *outputvector, fir_filter_status_multistage_t *fs)
{
  int i;

  i = 0;

  fs->statusvector[i].inputlen = fs->inputlen;
  fs->statusvector[i].outputlen = fs->bufferlen;
  FIRdownsample (inputvector, fs->buffer[i % 2], &fs->statusvector[i]);
  fs->inputcount = fs->statusvector[i].inputcount;



  for (i = 1; i < fs->filterstages - 1; i++)
  {
    fs->statusvector[i].inputlen = fs->statusvector[i - 1].outputcount;
    fs->statusvector[i].outputlen = fs->bufferlen;
    FIRdownsample (fs->buffer[(i - 1) % 2], fs->buffer[i % 2], &fs->statusvector[i]);
  }
  fs->statusvector[i].inputlen = fs->statusvector[i - 1].outputcount;
  fs->statusvector[i].outputlen = fs->outputlen;
  FIRdownsample (fs->buffer[(i - 1) % 2], outputvector, &fs->statusvector[i]);
  fs->outputcount = fs->statusvector[i].outputcount;
  return fs->outputcount;
}


