#ifndef ETHERCAT_CONFIG_H
#define ETHERCAT_CONFIG_H

#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "ecrt.h"


// Application parameters
#define FREQUENCY 1000
#define HEARTBEET 10

#define MEASURE_TIMING

#define ETHERCAT_EPOCH_OFFSET 946684800  // output of date -u -d '1/1/2000' +%s

#define ELM3004_FSV 7812500;

/****************************************************************************/

#define NSEC_PER_SEC (1000000000L)
#define PERIOD_NS (NSEC_PER_SEC / FREQUENCY)
#define PERIOD_HEARTBEET (NSEC_PER_SEC / HEARTBEET)


#define DIFF_NS(A, B) (((B).tv_sec - (A).tv_sec) * NSEC_PER_SEC + \
	(B).tv_nsec - (A).tv_nsec)

#define TIMESPEC2NS(T) ((uint64_t) (T).tv_sec * NSEC_PER_SEC + (T).tv_nsec)



#define Beckhoff_EK1501       0x00000002, 0x05dd2c52
#define Beckhoff_EL3751       0x00000002, 0x0ea73052
#define Beckhoff_EL7211       0x00000002, 0x1c2b3052
#define Beckhoff_EK1100       0x00000002, 0x044c2c52
#define Beckhoff_EK1101       0x00000002, 0x044d2c52
#define Beckhoff_EL5021       0x00000002, 0x139d3052
#define Beckhoff_EL3751       0x00000002, 0x0ea73052
#define Beckhoff_EL7041       0x00000002, 0x1b813052
#define Beckhoff_ELM3004      0x00000002, 0x50216dc9
#define Beckhoff_ELM3148      0x00000002, 0x502176c9 
#define Beckhoff_EL2004       0x00000002, 0x07d43052
#define Beckhoff_EL1004       0x00000002, 0x03ec3052
#define Beckhoff_EL1014       0x00000002, 0x03f63052
#define Beckhoff_EL4134       0x00000002, 0x10263052



typedef enum
{
  initial,
  set_enable,
  ready,
  stop,
  error1,
  error2
} initstate_t;

typedef enum
{
  wait0,
  move_idle,
  preparehoming,
  waithoming,
  preparemove,
  waitmove,
  error
} movingstate_t;





typedef struct
{
  int o_enab;
  int o_enable;
  int o_exec;
  int o_startt;

  int o_target;
  int64_t targpos;

  int64_t actvel;

  int o_velo;
  int64_t velocity;

  int o_status;
  int status;

  int o_din;
  int din;

  int o_actpos;
  int64_t actpos;

  int o_actvel;

  int o_actdrive;
  int64_t actdrive;

  int o_intpos;
  int64_t intpos;

  int o_encpos;
  int64_t encpos;

  int o_moveactive;
  int moveactive;

  int homed;

  initstate_t initstate;
  movingstate_t movestate;

//  int64_t lastcounter;

  int64_t state;
  int64_t command;

  double cgoal;
  double cepos;
  double cmpos;

  double vel;
  double scale;
  double offset;

  int overallstat;

  char statusstr[1024];

} EL7041vars_t;



typedef struct
{
  int o_num[4];
  int o_ain[4][5];
    
  int8_t  num[4];
  int32_t val[4][5];
  int64_t ainc[4];
  double  av_val[4];
  double  range;
  
} ELM3004vars_t;



typedef struct
{
  int o_val[4];
  int16_t val[4];
} EL4134vars_t;


typedef struct
{
  int o_num[8];
  int o_val[8][5];
  
  int8_t  num[8];
  int32_t val[8][5];
  int64_t ainc[8];
  
} ELM3148vars_t;



typedef struct
{
  int o_out;
  int val;
} EL2004vars_t;


typedef struct
{
  int o_in;
  int val;
} EL10X4vars_t;



struct timespec timespec_add(struct timespec time1, struct timespec time2);
int EL7041config (int nmaster, int nslave, ec_master_t *master, ec_domain_t *domain, EL7041vars_t *var);
int EL7041readvar (uint8_t *domain_pd, EL7041vars_t *var);

int EL4134config(int nmaster, int nslave, ec_master_t *master, ec_domain_t *domain, EL4134vars_t *var);
int EL4134writevar (uint8_t *domain_pd, EL4134vars_t *var);


int ELM3004config(int nmaster, int nslave, ec_master_t *master, ec_domain_t *domain, ELM3004vars_t *var);
int ELM3004readvar (uint8_t *domain_pd, ELM3004vars_t *var);

int ELM3148config(int nmaster, int nslave, ec_master_t *master, ec_domain_t *domain, ELM3148vars_t *var);
int ELM3148readvar (uint8_t *domain_pd, ELM3148vars_t *var);

int EL2004config(int nmaster, int nslave, ec_master_t *master, ec_domain_t *domain, EL2004vars_t *var);

int EL1004config(int nmaster, int nslave, ec_master_t *master, ec_domain_t *domain, EL10X4vars_t *var);

int EL1014config(int nmaster, int nslave, ec_master_t *master, ec_domain_t *domain, EL10X4vars_t *var);


int EK1101config (int nmaster, int nslave, ec_master_t *master);



#endif

