#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>

//#include <iostream>
//#include <fstream>
#include <math.h>

//#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

//#include <scpi/scpi.h>

//#include <stdint.h>
//#include <malloc.h> 
//#include <time.h>
//#include <sys/time.h>
//#include <sys/resource.h>
//#include <sys/stat.h>
#include <sys/mman.h>
//#include <unistd.h>
#include <fcntl.h>

#include "gradiometer_ipc.h"


#define GIT_REV "123"

/// fuction to calulate the checusum IPC
/// inspired by: https://en.wikipedia.org/wiki/Fletcher's_checksum

uint64_t Fletcher64( void *data, int bytecount )
{
  uint32_t *data32 = (uint32_t *)data;

  uint64_t sum1 = 0;
  uint64_t sum2 = 0;

  int index;

  for( index = 0; index < bytecount / 4; ++index )
  {
    sum1 = (sum1 + data32[index]) % CHECKSUM_PRIME;
    sum2 = (sum2 + sum1) % CHECKSUM_PRIME;
  }
  return (sum2 << 32) | sum1;
}


/// time in seconds as a double with ns resolution
double dtime(void)
{
  struct timespec ntime;
  clock_gettime(CLOCK_REALTIME, &ntime);
  return (double)ntime.tv_sec + ntime.tv_nsec / 1000000000.;
}


int create_listen_tcp_socket (int port)
{

  /* for setsockopt() SO_REUSEADDR, below */
  int yes = 1;
  //  socklen_t addrlen;

  /* server address */
  struct sockaddr_in serveraddr;

  int listener;

  /* get the listener */

  if((listener = socket(AF_INET, SOCK_STREAM, 0)) == -1)
  {
    perror("Server-socket() error!");
    /*just exit lol!*/
    exit(1);
  }

  /*"address already in use" error message */

  if(setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1)
  {
    perror("Server-setsockopt() error!");
    exit(1);
  }

  /* bind */

  serveraddr.sin_family = AF_INET;
  serveraddr.sin_addr.s_addr = INADDR_ANY;
  serveraddr.sin_port = htons(port);
  memset(&(serveraddr.sin_zero), '\0', 8);

  if(bind(listener, (struct sockaddr *)&serveraddr, sizeof(serveraddr)) == -1)
  {
    perror("Server-bind() error lol!");
    exit(1);
  }

  /* listen */

  if(listen(listener, 10) == -1)
  {
    perror("Server-listen() error lol!");
    exit(1);
  }

  printf("Listening on port %d \n", port);

  return listener;

}





/*! Function to initialize the inter process communication via memory mapped file.
 *
 *
 */


int IPC_init (ipc_workdata_t *pwd, int create)
{

  int result;
  bool newfile = false;

  // ******  prepare Inter Process Communication via a mapped file *******************

	//printf("IPC_FILE = %s\n", IPC_FILE);
  pwd->ipc_fd = open(IPC_FILE, O_RDWR);

  if (pwd->ipc_fd == -1) // file does probably not exist -> let's try to create it
  {
    if (create)
    {
      printf("IPC file \"%s\" not found. creating new file\n", IPC_FILE);

      printf("open \n");
      pwd->ipc_fd = open(IPC_FILE, O_RDWR | O_CREAT | O_TRUNC, (mode_t)0777);//was 0600

      if (pwd->ipc_fd == -1)
      {
        perror("Error opening file for writing");
        exit(EXIT_FAILURE);
      }


      /* Stretch the file size to the size of the (mmapped) array of ints
      */
      printf("seek \n");
      result = lseek(pwd->ipc_fd, sizeof(ipc_map_t) -1, SEEK_SET);
      if (result == -1)
      {
        close(pwd->ipc_fd);
        perror("Error calling lseek() to 'stretch' the file");
        exit(EXIT_FAILURE);
      }


      /* Something needs to be written at the end of the file */
      printf("write  \n");
      result = write(pwd->ipc_fd, "", 1);
      if (result != 1)
      {
        close(pwd->ipc_fd);
        perror("Error writing last byte of the file");
        exit(EXIT_FAILURE);
      }


      newfile = true;

    } // create new IPC file


    else
    {
      perror("IPC file cannot be opened - IPC_init.\n ");
      exit(EXIT_FAILURE);
    }
  }

  // map the IPC file to memoryconfig
printf("File opened\n");
  pwd->map = (ipc_map_t*) mmap(0, sizeof(ipc_map_t), PROT_READ | PROT_WRITE,
                               MAP_SHARED, pwd->ipc_fd, 0);

  if (pwd->map == MAP_FAILED)
  {
    close(pwd->ipc_fd);
    perror("Error mapping the IPC file.\n");
    exit(EXIT_FAILURE);
  }



  if (newfile)
  {
    printf("memset  \n");
    memset(pwd->map, 0, sizeof(ipc_map_t));

    //pwd->map->versionchecksum = Fletcher64( (void *) GIT_REV, strlen(GIT_REV)); // set the versionchecksum based on GIT_REV
    printf("using git hash %s and version checklsum %" PRIu64 "\n", GIT_REV, pwd->map->versionchecksum);
  }
  else
  {
    if ( pwd->map->versionchecksum != Fletcher64( (void *) GIT_REV,
         strlen(GIT_REV)) ) // set the versionchecksum does not match
    {
      perror("Version checksum of the IPC file dos not match. Try to delete and recreate the IPC file.\n");
      exit(EXIT_FAILURE);
    }

  }
  return newfile;
}



void IPC_write_fbd (ipc_workdata_t *pwd )
{
  memcpy(&(pwd->map->feedback_data), &pwd->fbd, sizeof(feedback_data_t));
  pwd->map->fdchecksum = Fletcher64( &pwd->fbd, sizeof(feedback_data_t));
}

int IPC_read_fbd (ipc_workdata_t *pwd)
{
  feedback_data_t fbdcopy; ///< local copy of the feedback data;
  memcpy(&fbdcopy, &(pwd->map->feedback_data), sizeof(feedback_data_t));

  if (pwd->map->fdchecksum == Fletcher64( &fbdcopy, sizeof(feedback_data_t) ))
  {
    memcpy(&pwd->fbd, &fbdcopy, sizeof(feedback_data_t));
    return 0;
  }
  else
  {
   // printf("checksum Error\n");
    return -1;
  }
}

void IPC_write_cfd (ipc_workdata_t *pwd)
{
  memcpy(&(pwd->map->config_data), &pwd->cfd, sizeof(config_data_t));
  pwd->map->cdchecksum = Fletcher64( &pwd->cfd, sizeof(config_data_t));
}

int IPC_read_cfd (ipc_workdata_t *pwd)
{

  config_data_t cfdcopy; ///< local copy of the feedback data;
  memcpy(&cfdcopy, &(pwd->map->config_data), sizeof(config_data_t));

  if (pwd->map->cdchecksum == Fletcher64( &cfdcopy, sizeof(config_data_t) ))
  {
    memcpy(&pwd->cfd, &cfdcopy, sizeof(config_data_t));
    return 0;
  }
  else
  {
    // printf("checksum Error\n");
    return -1;
  }
}






