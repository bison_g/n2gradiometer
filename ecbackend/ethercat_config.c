#include "ethercat_config.h"



int EL1004config(int nmaster, int nslave, ec_master_t *master, ec_domain_t *domain, EL10X4vars_t *var)
{
  ec_slave_config_t *sc;

  ec_pdo_entry_info_t slave_10_pdo_entries[] =
  {
    {0x6000, 0x01, 1}, /* Input */
    {0x6010, 0x01, 1}, /* Input */
    {0x6020, 0x01, 1}, /* Input */
    {0x6030, 0x01, 1}, /* Input */
  };

  ec_pdo_info_t slave_10_pdos[] =
  {
    {0x1a00, 1, slave_10_pdo_entries + 0}, /* Channel 1 */
    {0x1a01, 1, slave_10_pdo_entries + 1}, /* Channel 2 */
    {0x1a02, 1, slave_10_pdo_entries + 2}, /* Channel 3 */
    {0x1a03, 1, slave_10_pdo_entries + 3}, /* Channel 4 */
  };

  ec_sync_info_t slave_10_syncs[] =
  {
    {0, EC_DIR_INPUT, 4, slave_10_pdos + 0, EC_WD_DISABLE},
    {0xff}
  };



  int ret;
  unsigned int bitposition;

  if (!(sc = ecrt_master_slave_config(master, nmaster, nslave, Beckhoff_EL1004)))
  {
    fprintf(stderr, "Failed to get slave configuration.\n");
    return -1;
  }

  ret = ecrt_slave_config_pdos(sc, EC_END, slave_10_syncs);
  if (ret)
  {
    return (ret);
  }
  if ((var->o_in = ecrt_slave_config_reg_pdo_entry(sc, 0x6000, 0x01, domain, &bitposition)) < 0)
  {
    printf("el2004 config %d, bitpos %d\n", var->o_in, bitposition);
    return (-1);
  }
  printf("el2004 config %d, bitpos %d\n", var->o_in, bitposition);

  return 0;

}


int EL1014config(int nmaster, int nslave, ec_master_t *master, ec_domain_t *domain, EL10X4vars_t *var)
{
  ec_slave_config_t *sc;


ec_pdo_entry_info_t slave_6_pdo_entries[] = {
    {0x6000, 0x01, 1}, /* Input */
    {0x6010, 0x01, 1}, /* Input */
    {0x6020, 0x01, 1}, /* Input */
    {0x6030, 0x01, 1}, /* Input */
};

ec_pdo_info_t slave_6_pdos[] = {
    {0x1a00, 1, slave_6_pdo_entries + 0}, /* Channel 1 */
    {0x1a01, 1, slave_6_pdo_entries + 1}, /* Channel 2 */
    {0x1a02, 1, slave_6_pdo_entries + 2}, /* Channel 3 */
    {0x1a03, 1, slave_6_pdo_entries + 3}, /* Channel 4 */
};

ec_sync_info_t slave_6_syncs[] = {
    {0, EC_DIR_INPUT, 4, slave_6_pdos + 0, EC_WD_DISABLE},
    {0xff}
};



  int ret;
  unsigned int bitposition;

  if (!(sc = ecrt_master_slave_config(master, nmaster, nslave, Beckhoff_EL1014)))
  {
    fprintf(stderr, "Failed to get slave configuration.\n");
    return -1;
  }

  ret = ecrt_slave_config_pdos(sc, EC_END, slave_6_syncs);
  if (ret)
  {
    return (ret);
  }
  if ((var->o_in = ecrt_slave_config_reg_pdo_entry(sc, 0x6000, 0x01, domain, &bitposition)) < 0)
  {
    printf("el2004 config %d, bitpos %d\n", var->o_in, bitposition);
    return (-1);
  }
  printf("el2004 config %d, bitpos %d\n", var->o_in, bitposition);

  return 0;

}




int EL2004config(int nmaster, int nslave, ec_master_t *master, ec_domain_t *domain, EL2004vars_t *var)
{
  ec_slave_config_t *sc;

  ec_pdo_entry_info_t slave_9_pdo_entries[] =
  {
    {0x7000, 0x01, 1}, /* Output */
    {0x7010, 0x01, 1}, /* Output */
    {0x7020, 0x01, 1}, /* Output */
    {0x7030, 0x01, 1}, /* Output */
  };

  ec_pdo_info_t slave_9_pdos[] =
  {
    {0x1600, 1, slave_9_pdo_entries + 0}, /* Channel 1 */
    {0x1601, 1, slave_9_pdo_entries + 1}, /* Channel 2 */
    {0x1602, 1, slave_9_pdo_entries + 2}, /* Channel 3 */
    {0x1603, 1, slave_9_pdo_entries + 3}, /* Channel 4 */
  };

  ec_sync_info_t slave_9_syncs[] =
  {
    {0, EC_DIR_OUTPUT, 4, slave_9_pdos + 0, EC_WD_ENABLE},
    {0xff}
  };



  int ret;
  unsigned int bitposition;

  if (!(sc = ecrt_master_slave_config(master, nmaster, nslave, Beckhoff_EL2004)))
  {
    fprintf(stderr, "Failed to get slave configuration.\n");
    return -1;
  }


  ret = ecrt_slave_config_pdos(sc, EC_END, slave_9_syncs);
  if (ret)
  {
    return (ret);
  }

  if ((var->o_out = ecrt_slave_config_reg_pdo_entry(sc, 0x7000, 0x01, domain, &bitposition)) < 0)

  {
    printf("el1004 config %d, bitpos %d\n", var->o_out, bitposition);
    return (-1);
  }
  printf("el1004 config %d, bitpos %d\n", var->o_out, bitposition);
  return 0;

}

int EL4134config(int nmaster, int nslave, ec_master_t *master, ec_domain_t *domain, EL4134vars_t *var)
{
  ec_slave_config_t *sc;


/* Master 0, Slave 5, "EL4134"
 * Vendor ID:       0x00000002
 * Product code:    0x10263052
 * Revision number: 0x03fa0000
 */

ec_pdo_entry_info_t slave_5_pdo_entries[] = {
    {0x7000, 0x01, 16}, /* Analog output */
    {0x7010, 0x01, 16}, /* Analog output */
    {0x7020, 0x01, 16}, /* Analog output */
    {0x7030, 0x01, 16}, /* Analog output */
};

ec_pdo_info_t slave_5_pdos[] = {
    {0x1600, 1, slave_5_pdo_entries + 0}, /* AO RxPDO-Map OutputsCh.1 */
    {0x1601, 1, slave_5_pdo_entries + 1}, /* AO RxPDO-Map OutputsCh.2 */
    {0x1602, 1, slave_5_pdo_entries + 2}, /* AO RxPDO-Map OutputsCh.3 */
    {0x1603, 1, slave_5_pdo_entries + 3}, /* AO RxPDO-Map OutputsCh.4 */
};

ec_sync_info_t slave_5_syncs[] = {
    {0, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
    {1, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},
    {2, EC_DIR_OUTPUT, 4, slave_5_pdos + 0, EC_WD_DISABLE},
    {3, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},
    {0xff}
};




  printf("EL4134 config\n");

  int ret;

  if (!(sc = ecrt_master_slave_config(master, nmaster, nslave, Beckhoff_EL4134)))
  {
    fprintf(stderr, "Failed to get slave configuration.\n");
    return -1;
  }

  ret = ecrt_slave_config_pdos(sc, EC_END, slave_5_syncs);
  if (ret)
  {
    printf("EL4134 config error!\n");
    return (ret);
  }


  for (int i = 0; i < 4; i++)
  {
    if ((var->o_val[i] = ecrt_slave_config_reg_pdo_entry(sc, 0x7000 + i * 0x10, 0x01, domain, NULL)) < 0)
    {
  	  printf("EL4134 config error!\n");
      return (-1);
    }
  }


  return 0;
}


int EL4134writevar (uint8_t *domain_pd, EL4134vars_t *var)
{
  for (int i = 0; i < 4; i++)
  {
    EC_WRITE_S16 (domain_pd + var->o_val[i],var->val[i]);    
 } 
  
  return 0;
}






int ELM3148config(int nmaster, int nslave, ec_master_t *master, ec_domain_t *domain, ELM3148vars_t *var)
{
  ec_slave_config_t *sc;


/* Master 0, Slave 7, "ELM3148-0000"
 * Vendor ID:       0x00000002
 * Product code:    0x502176c9
 * Revision number: 0x00130000
 */

ec_pdo_entry_info_t slave_7_pdo_entries[] = {
    {0x6000, 0x01, 8}, /* No of Samples */
    {0x6000, 0x09, 1}, /* Error */
    {0x6000, 0x0a, 1}, /* Underrange */
    {0x6000, 0x0b, 1}, /* Overrange */
    {0x0000, 0x00, 1}, /* Gap */
    {0x6000, 0x0d, 1}, /* Diag */
    {0x6000, 0x0e, 1}, /* TxPDO State */
    {0x6000, 0x0f, 2}, /* Input cycle counter */
    {0x0000, 0x00, 16}, /* Gap */
    {0x6001, 0x01, 32}, /* SubIndex 001 */
    {0x6005, 0x01, 32}, /* Low */
    {0x6005, 0x02, 32}, /* Hi */
    {0x6010, 0x01, 8}, /* No of Samples */
    {0x6010, 0x09, 1}, /* Error */
    {0x6010, 0x0a, 1}, /* Underrange */
    {0x6010, 0x0b, 1}, /* Overrange */
    {0x0000, 0x00, 1}, /* Gap */
    {0x6010, 0x0d, 1}, /* Diag */
    {0x6010, 0x0e, 1}, /* TxPDO State */
    {0x6010, 0x0f, 2}, /* Input cycle counter */
    {0x0000, 0x00, 16}, /* Gap */
    {0x6011, 0x01, 32}, /* SubIndex 001 */
    {0x6020, 0x01, 8}, /* No of Samples */
    {0x6020, 0x09, 1}, /* Error */
    {0x6020, 0x0a, 1}, /* Underrange */
    {0x6020, 0x0b, 1}, /* Overrange */
    {0x0000, 0x00, 1}, /* Gap */
    {0x6020, 0x0d, 1}, /* Diag */
    {0x6020, 0x0e, 1}, /* TxPDO State */
    {0x6020, 0x0f, 2}, /* Input cycle counter */
    {0x0000, 0x00, 16}, /* Gap */
    {0x6021, 0x01, 32}, /* SubIndex 001 */
    {0x6030, 0x01, 8}, /* No of Samples */
    {0x6030, 0x09, 1}, /* Error */
    {0x6030, 0x0a, 1}, /* Underrange */
    {0x6030, 0x0b, 1}, /* Overrange */
    {0x0000, 0x00, 1}, /* Gap */
    {0x6030, 0x0d, 1}, /* Diag */
    {0x6030, 0x0e, 1}, /* TxPDO State */
    {0x6030, 0x0f, 2}, /* Input cycle counter */
    {0x0000, 0x00, 16}, /* Gap */
    {0x6031, 0x01, 32}, /* SubIndex 001 */
    {0x6040, 0x01, 8}, /* No of Samples */
    {0x6040, 0x09, 1}, /* Error */
    {0x6040, 0x0a, 1}, /* Underrange */
    {0x6040, 0x0b, 1}, /* Overrange */
    {0x0000, 0x00, 1}, /* Gap */
    {0x6040, 0x0d, 1}, /* Diag */
    {0x6040, 0x0e, 1}, /* TxPDO State */
    {0x6040, 0x0f, 2}, /* Input cycle counter */
    {0x0000, 0x00, 16}, /* Gap */
    {0x6041, 0x01, 32}, /* SubIndex 001 */
    {0x6050, 0x01, 8}, /* No of Samples */
    {0x6050, 0x09, 1}, /* Error */
    {0x6050, 0x0a, 1}, /* Underrange */
    {0x6050, 0x0b, 1}, /* Overrange */
    {0x0000, 0x00, 1}, /* Gap */
    {0x6050, 0x0d, 1}, /* Diag */
    {0x6050, 0x0e, 1}, /* TxPDO State */
    {0x6050, 0x0f, 2}, /* Input cycle counter */
    {0x0000, 0x00, 16}, /* Gap */
    {0x6051, 0x01, 32}, /* SubIndex 001 */
    {0x6060, 0x01, 8}, /* No of Samples */
    {0x6060, 0x09, 1}, /* Error */
    {0x6060, 0x0a, 1}, /* Underrange */
    {0x6060, 0x0b, 1}, /* Overrange */
    {0x0000, 0x00, 1}, /* Gap */
    {0x6060, 0x0d, 1}, /* Diag */
    {0x6060, 0x0e, 1}, /* TxPDO State */
    {0x6060, 0x0f, 2}, /* Input cycle counter */
    {0x0000, 0x00, 16}, /* Gap */
    {0x6061, 0x01, 32}, /* SubIndex 001 */
    {0x6070, 0x01, 8}, /* No of Samples */
    {0x6070, 0x09, 1}, /* Error */
    {0x6070, 0x0a, 1}, /* Underrange */
    {0x6070, 0x0b, 1}, /* Overrange */
    {0x0000, 0x00, 1}, /* Gap */
    {0x6070, 0x0d, 1}, /* Diag */
    {0x6070, 0x0e, 1}, /* TxPDO State */
    {0x6070, 0x0f, 2}, /* Input cycle counter */
    {0x0000, 0x00, 16}, /* Gap */
    {0x6071, 0x01, 32}, /* SubIndex 001 */
};

ec_pdo_info_t slave_7_pdos[] = {
    {0x1a00, 9, slave_7_pdo_entries + 0}, /* PAI TxPDO-Map Status Ch.1 */
    {0x1a01, 1, slave_7_pdo_entries + 9}, /* PAI TxPDO-Map Samples 1 Ch.1 */
    {0x1a09, 2, slave_7_pdo_entries + 10}, /* PAI TxPDO-Map Timestamp Ch.1 */
    {0x1a0b, 9, slave_7_pdo_entries + 12}, /* PAI TxPDO-Map Status Ch.2 */
    {0x1a0c, 1, slave_7_pdo_entries + 21}, /* PAI TxPDO-Map Samples 1 Ch.2 */
    {0x1a16, 9, slave_7_pdo_entries + 22}, /* PAI TxPDO-Map Status Ch.3 */
    {0x1a17, 1, slave_7_pdo_entries + 31}, /* PAI TxPDO-Map Samples 1 Ch.3 */
    {0x1a21, 9, slave_7_pdo_entries + 32}, /* PAI TxPDO-Map Status Ch.4 */
    {0x1a22, 1, slave_7_pdo_entries + 41}, /* PAI TxPDO-Map Samples 1 Ch.4 */
    {0x1a2c, 9, slave_7_pdo_entries + 42}, /* PAI TxPDO-Map Status Ch.5 */
    {0x1a2d, 1, slave_7_pdo_entries + 51}, /* PAI TxPDO-Map Samples 1 Ch.5 */
    {0x1a37, 9, slave_7_pdo_entries + 52}, /* PAI TxPDO-Map Status Ch.6 */
    {0x1a38, 1, slave_7_pdo_entries + 61}, /* PAI TxPDO-Map Samples 1 Ch.6 */
    {0x1a42, 9, slave_7_pdo_entries + 62}, /* PAI TxPDO-Map Status Ch.7 */
    {0x1a43, 1, slave_7_pdo_entries + 71}, /* PAI TxPDO-Map Samples 1 Ch.7 */
    {0x1a4d, 9, slave_7_pdo_entries + 72}, /* PAI TxPDO-Map Status Ch.8 */
    {0x1a4e, 1, slave_7_pdo_entries + 81}, /* PAI TxPDO-Map Samples 1 Ch.8 */
};

ec_sync_info_t slave_7_syncs[] = {
    {0, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
    {1, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},
    {2, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
    {3, EC_DIR_INPUT, 17, slave_7_pdos + 0, EC_WD_DISABLE},
    {0xff}
};



  printf("ELM3148 config\n");

  int ret;

  if (!(sc = ecrt_master_slave_config(master, nmaster, nslave, Beckhoff_ELM3148)))
  {
    fprintf(stderr, "Failed to get slave configuration.\n");
    return -1;
  }


  ret = ecrt_slave_config_pdos(sc, EC_END, slave_7_syncs);
  if (ret)
  {
    printf("ELM3148 config error!\n");
    return (ret);
  }

//  ecrt_slave_config_dc(sc, 0x0700, PERIOD_NS, 0, 0, 0);

  for (int i = 0; i < 8; i++)
  {

    if ((var->o_num[i] = ecrt_slave_config_reg_pdo_entry(sc, 0x6000 + i * 0x10, 0x01, domain, NULL)) < 0)
    {
  	  printf("ELM3148 config error!\n");
      return (-1);
    }
    printf("n:%d, ",var->o_num[i]);
    var->ainc[i] =0;
    
    for (int j = 0; j < 1; j++)
    {
      if ((var->o_val[i][j] = ecrt_slave_config_reg_pdo_entry(sc, 0x6001 + i * 0x10, j + 1, domain, NULL)) < 0)
      {
  	    printf("ELM3148 config error!\n");
        return (-1);
      }
       printf("v:%d, ",var->o_val[i][j]);

    }
  }
  
  printf("ELM3148 config ready\n");

  return 0;
}





int ELM3148readvar (uint8_t *domain_pd, ELM3148vars_t *var)
{
  for (int i = 0; i < 8; i++)
  {
    var->num[i]=EC_READ_U8 (domain_pd + var->o_num[i]);
    
    for (int j = 0; j < var->num[i] ; j++)
    {
      var->val[i][j] = EC_READ_S32(domain_pd + var->o_val[i][j]);     
      var->ainc[i]++; 
    }
    
 } 
  
  return 0;
}






int ELM3004config(int nmaster, int nslave, ec_master_t *master, ec_domain_t *domain, ELM3004vars_t *var)
{
  ec_slave_config_t *sc;

  ec_pdo_entry_info_t ELM3004_pdo_entries[] =
  {
    {0x6000, 0x01, 8},
    {0x6000, 0x09, 1},
    {0x6000, 0x0a, 1},
    {0x6000, 0x0b, 1},
    {0x0000, 0x00, 1},
    {0x6000, 0x0d, 1},
    {0x6000, 0x0e, 1},
    {0x6000, 0x0f, 2},
    {0x0000, 0x00, 16},
    {0x6001, 0x01, 32},
    {0x6001, 0x02, 32},
    {0x6001, 0x03, 32},
    {0x6001, 0x04, 32},
    {0x6001, 0x05, 32},
    {0x6010, 0x01, 8},
    {0x6010, 0x09, 1},
    {0x6010, 0x0a, 1},
    {0x6010, 0x0b, 1},
    {0x0000, 0x00, 1},
    {0x6010, 0x0d, 1},
    {0x6010, 0x0e, 1},
    {0x6010, 0x0f, 2},
    {0x0000, 0x00, 16},
    {0x6011, 0x01, 32},
    {0x6011, 0x02, 32},
    {0x6011, 0x03, 32},
    {0x6011, 0x04, 32},
    {0x6011, 0x05, 32},
    {0x6020, 0x01, 8},
    {0x6020, 0x09, 1},
    {0x6020, 0x0a, 1},
    {0x6020, 0x0b, 1},
    {0x0000, 0x00, 1},
    {0x6020, 0x0d, 1},
    {0x6020, 0x0e, 1},
    {0x6020, 0x0f, 2},
    {0x0000, 0x00, 16},
    {0x6021, 0x01, 32},
    {0x6021, 0x02, 32},
    {0x6021, 0x03, 32},
    {0x6021, 0x04, 32},
    {0x6021, 0x05, 32},
    {0x6030, 0x01, 8},
    {0x6030, 0x09, 1},
    {0x6030, 0x0a, 1},
    {0x6030, 0x0b, 1},
    {0x0000, 0x00, 1},
    {0x6030, 0x0d, 1},
    {0x6030, 0x0e, 1},
    {0x6030, 0x0f, 2},
    {0x0000, 0x00, 16},
    {0x6031, 0x01, 32},
    {0x6031, 0x02, 32},
    {0x6031, 0x03, 32},
    {0x6031, 0x04, 32},
    {0x6031, 0x05, 32},
  };

  ec_pdo_info_t ELM3004_pdos[] =
  {
    {0x1a00, 9, ELM3004_pdo_entries + 0}, /* PAI TxPDO-Map Status Ch.1 */
    {0x1a04, 5, ELM3004_pdo_entries + 9}, /* PAI TxPDO-Map Samples 5 Ch.1 */
    {0x1a21, 9, ELM3004_pdo_entries + 14}, /* PAI TxPDO-Map Status Ch.2 */
    {0x1a25, 5, ELM3004_pdo_entries + 23}, /* PAI TxPDO-Map Samples 5 Ch.2 */
    {0x1a42, 9, ELM3004_pdo_entries + 28}, /* PAI TxPDO-Map Status Ch.3 */
    {0x1a46, 5, ELM3004_pdo_entries + 37}, /* PAI TxPDO-Map Samples 5 Ch.3 */
    {0x1a63, 9, ELM3004_pdo_entries + 42}, /* PAI TxPDO-Map Status Ch.4 */
    {0x1a67, 5, ELM3004_pdo_entries + 51}, /* PAI TxPDO-Map Samples 5 Ch.4 */
  };

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"

  ec_sync_info_t ELM3004_syncs[] =
  {
    {0, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
    {1, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},
    {2, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
    {3, EC_DIR_INPUT, 8, ELM3004_pdos + 0, EC_WD_DISABLE},
    {0xff}
  };

#pragma GCC diagnostic pop


  int ret;

  if (!(sc = ecrt_master_slave_config(master, nmaster, nslave, Beckhoff_ELM3004)))
  {
    fprintf(stderr, "Failed to get slave configuration.\n");
    return -1;
  }


  ret = ecrt_slave_config_pdos(sc, EC_END, ELM3004_syncs);
  if (ret)
  {
    return (ret);
  }

 ecrt_slave_config_dc(sc, 0x0700, PERIOD_NS, 4400000, 0, 0);

  for (int i = 0; i < 4; i++)
  {

    if ((var->o_num[i] = ecrt_slave_config_reg_pdo_entry(sc, 0x6000 + i * 0x10, 0x01, domain, NULL)) < 0)
    {
      return (-1);
    }

    for (int j = 0; j < 5; j++)
    {

      if ((var->o_ain[i][j] = ecrt_slave_config_reg_pdo_entry(sc, 0x6001 + i * 0x10, j + 1, domain, NULL)) < 0)
      {
        return (-1);
      }
    } 
    /*
    ret += ecrt_slave_config_sdo16(sc, 0x8000+i*0x10, 0x01, 9); // set range to Â±80 mV
    var->range=0.080;

    ret += ecrt_slave_config_sdo16(sc, 0x8000+i*0x10, 0x01, 8); // set range to Â±160 mV
    var->range=0.160;

    ret += ecrt_slave_config_sdo16(sc, 0x8000+i*0x10, 0x01, 7); // set range to Â±320 mV
    var->range=0.320;

    ret += ecrt_slave_config_sdo16(sc, 0x8000+i*0x10, 0x01, 6); // set range to Â±640 mV
    var->range=0.640;

    ret += ecrt_slave_config_sdo16(sc, 0x8000+i*0x10, 0x01, 5); // set range to Â±1250 mV
    var->range=1.25;

    ret += ecrt_slave_config_sdo16(sc, 0x8000+i*0x10, 0x01, 4); // set range to Â±2500 mV
    var->range=2.5;

    ret += ecrt_slave_config_sdo16(sc, 0x8000+i*0x10, 0x01, 3); // set range to Â±5000 mV
    var->range=5.;

    ret += ecrt_slave_config_sdo16(sc, 0x8000+i*0x10, 0x01, 2); // set range to Â±10V
    var->range=10.;
    * 
    * ret += ecrt_slave_config_sdo16(sc, 0x8000+i*0x10, 0x01, 1); // set range to Â±30V
    * 
    */
        
    ret += ecrt_slave_config_sdo16(sc, 0x8000+i*0x10, 0x01, 2); // set range to Â±10V
    var->range=10.;
   
    ret += ecrt_slave_config_sdo16(sc, 0x8000 + i * 0x10, 0x2E, 0); // set range to Â±extended range

    ret += ecrt_slave_config_sdo16(sc, 0x8000 + i * 0x10, 0x16, 22); // set 1kHz IIR filter
    ret += ecrt_slave_config_sdo16(sc, 0x8000 + i * 0x10, 0x18, 1); // set decimation to 2

 

  }
  
  printf("ELM3004 config OK\n");
  return 0;
}






int ELM3004readvar (uint8_t *domain_pd, ELM3004vars_t *var)
{
	  
  for (int i = 0; i < 4; i++)
  {
    var->num[i]=EC_READ_U8 (domain_pd + var->o_num[i]);
    var->av_val[i] =0;
    
    if (var->num[i]>0)
    {
      for (int j = 0; j < var->num[i] ; j++)
      {
        var->val[i][j] = EC_READ_S32(domain_pd + var->o_ain[i][j]);         
//      printf("i:%2d,%2d, j:%2d, val:%6d, ",i,var->num[i], j,var->val[i][j]);    
        var->ainc[i]++; 
        var->av_val[i]+=var->val[i][j];
      }
      var->av_val[i]/=var->num[i];
      
      
//      printf("%d:")
      
    }
//    printf("\n");
  } 
  
 // printf("%10.1f\n",var->av_val[0]);
  return 0;
}









int EL7041config (int nmaster, int nslave, ec_master_t *master, ec_domain_t *domain, EL7041vars_t *var)
{
  ec_slave_config_t *sc;

  static ec_pdo_entry_info_t EL7041_pdo_entries[] =
  {
    {0x7000, 0x01, 1}, /* Enable latch C */
    {0x7000, 0x02, 1}, /* Enable latch extern on positive edge */
    {0x7000, 0x03, 1}, /* Set counter */
    {0x7000, 0x04, 1}, /* Enable latch extern on negative edge */
    {0x0000, 0x00, 4}, /* Gap */
    {0x0000, 0x00, 8}, /* Gap */
    {0x7000, 0x11, 32}, /* Set counter value */
    {0x7010, 0x01, 1}, /* Enable */
    {0x7010, 0x02, 1}, /* Reset */
    {0x7010, 0x03, 1}, /* Reduce torque */
    {0x0000, 0x00, 5}, /* Gap */
    {0x0000, 0x00, 8}, /* Gap */
    {0x7020, 0x01, 1}, /* Execute */
    {0x7020, 0x02, 1}, /* Emergency stop */
    {0x0000, 0x00, 6}, /* Gap */
    {0x0000, 0x00, 8}, /* Gap */
    {0x7020, 0x11, 32}, /* Target position */
    {0x7020, 0x21, 16}, /* Velocity */
    {0x7020, 0x22, 16}, /* Start type */
    {0x7020, 0x23, 16}, /* Acceleration */
    {0x7020, 0x24, 16}, /* Deceleration */
    {0x6000, 0x01, 1}, /* Latch C valid */
    {0x6000, 0x02, 1}, /* Latch extern valid */
    {0x6000, 0x03, 1}, /* Set counter done */
    {0x6000, 0x04, 1}, /* Counter underflow */
    {0x6000, 0x05, 1}, /* Counter overflow */
    {0x0000, 0x00, 2}, /* Gap */
    {0x6000, 0x08, 1}, /* Extrapolation stall */
    {0x6000, 0x09, 1}, /* Status of input A */
    {0x6000, 0x0a, 1}, /* Status of input B */
    {0x6000, 0x0b, 1}, /* Status of input C */
    {0x0000, 0x00, 1}, /* Gap */
    {0x6000, 0x0d, 1}, /* Status of extern latch */
    {0x6000, 0x0e, 1}, /* Sync error */
    {0x0000, 0x00, 1}, /* Gap */
    {0x6000, 0x10, 1}, /* TxPDO Toggle */
    {0x6000, 0x11, 32}, /* Counter value */
    {0x6000, 0x12, 32}, /* Latch value */
    {0x6010, 0x01, 1}, /* Ready to enable */
    {0x6010, 0x02, 1}, /* Ready */
    {0x6010, 0x03, 1}, /* Warning */
    {0x6010, 0x04, 1}, /* Error */
    {0x6010, 0x05, 1}, /* Moving positive */
    {0x6010, 0x06, 1}, /* Moving negative */
    {0x6010, 0x07, 1}, /* Torque reduced */
    {0x0000, 0x00, 1}, /* Gap */
    {0x0000, 0x00, 3}, /* Gap */
    {0x6010, 0x0c, 1}, /* Digital input 1 */
    {0x6010, 0x0d, 1}, /* Digital input 2 */
    {0x6010, 0x0e, 1}, /* Sync error */
    {0x0000, 0x00, 1}, /* Gap */
    {0x6010, 0x10, 1}, /* TxPDO Toggle */
    {0x6020, 0x01, 1}, // Status Busy
    {0x6020, 0x02, 1}, // Status In Target
    {0x6020, 0x03, 1}, // Status Warning
    {0x6020, 0x04, 1}, // Status Error
    {0x6020, 0x05, 1}, // Status Calibrated
    {0x6020, 0x06, 1}, // Status Accelerate
    {0x6020, 0x07, 1}, // Status Decelerate
    {0x0000, 0x00, 1}, // GAP
    {0x0000, 0x00, 8}, // GAP
    {0x6020, 0x11, 32}, // Actual Position
    {0x6020, 0x21, 16}, // Actual Velocity
    {0x6020, 0x22, 32}, // Actual drive time
    {0x6010, 0x14, 32}, /* Internal osition */
    {0x6010, 0x15, 32}, /* Encoder position */
  };

  static ec_pdo_info_t EL7041_pdos[] =
  {
    {0x1601, 7, EL7041_pdo_entries + 0}, /* ENC RxPDO-Map Control */
    {0x1602, 5, EL7041_pdo_entries + 7}, /* STM RxPDO-Map Control */
    {0x1606, 9, EL7041_pdo_entries + 12}, /* STM RxPDO-Map Position */
    {0x1a01, 17, EL7041_pdo_entries + 21}, /* ENC TxPDO-Map Status */
    {0x1a03, 14, EL7041_pdo_entries + 38}, /* STM TxPDO-Map Status */
    {0x1a07, 1, EL7041_pdo_entries + 52}, /* STM TxPDO-Map Status */
    {0x1a06, 12, EL7041_pdo_entries + 53}, /* STM TxPDO-Map Status */
    {0x1a08, 1, EL7041_pdo_entries + 65}, /* STM TxPDO-Map Status */
  };

  const static ec_sync_info_t EL7041_syncs[] =
  {
    {0, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
    {1, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},
    {2, EC_DIR_OUTPUT, 3, EL7041_pdos + 0, EC_WD_DISABLE},
    {3, EC_DIR_INPUT, 4, EL7041_pdos + 3, EC_WD_DISABLE},
    {0xff, EC_DIR_OUTPUT, 0, NULL, EC_WD_ENABLE}
  };


  if (!(sc = ecrt_master_slave_config(master, nmaster, nslave, Beckhoff_EL7041)))
  {
    fprintf(stderr, "Failed to get slave configuration.\n");
    return -1;
  }



  int ret;

  ret = ecrt_slave_config_pdos(sc, EC_END, EL7041_syncs);
  if (ret)
  {
    return (ret);
  }

  ecrt_slave_config_dc(sc, 0x0300, PERIOD_NS, 100000, 0, 0);

  if ((var->o_enab  = ecrt_slave_config_reg_pdo_entry(sc, 0x7010, 0x01, domain, NULL)) < 0)
  {
    return (-1);
  }

  if ((var->o_target    = ecrt_slave_config_reg_pdo_entry(sc, 0x7020, 0x11, domain, NULL)) < 0)
  {
    return (-1);
  }

  if ((var->o_velo    = ecrt_slave_config_reg_pdo_entry(sc, 0x7020, 0x21, domain, NULL)) < 0)
  {
    return (-1);
  }

  if ((var->o_startt  = ecrt_slave_config_reg_pdo_entry(sc, 0x7020, 0x22, domain, NULL)) < 0)
  {
    return (-1);
  }

  if ((var->o_exec  = ecrt_slave_config_reg_pdo_entry(sc, 0x7020, 0x01, domain, NULL)) < 0)
  {
    return (-1);
  }

  if ((var->o_status    = ecrt_slave_config_reg_pdo_entry(sc, 0x6010, 0x01, domain, NULL)) < 0)
  {
    return (-1);
  }

  if ((var->o_enable  = ecrt_slave_config_reg_pdo_entry(sc, 0x7010, 0x01, domain, NULL)) < 0)
  {
    return (-1);
  }

  if ((var->o_actpos  = ecrt_slave_config_reg_pdo_entry(sc, 0x6020, 0x11, domain, NULL)) < 0)
  {
    return (-1);
  }

  if ((var->o_actvel  = ecrt_slave_config_reg_pdo_entry(sc, 0x6020, 0x21, domain, NULL)) < 0)
  {
    return (-1);
  }

  if ((var->o_actdrive  = ecrt_slave_config_reg_pdo_entry(sc, 0x6020, 0x22, domain, NULL)) < 0)
  {
    return (-1);
  }

  if ((var->o_intpos = ecrt_slave_config_reg_pdo_entry(sc, 0x6010, 0x14, domain, NULL)) < 0)
  {
    return (-1);
  }

  if ((var->o_moveactive  = ecrt_slave_config_reg_pdo_entry(sc, 0x6020, 0x01, domain, NULL)) < 0)
  {
    return (-1);
  }

  if ((var->o_encpos  = ecrt_slave_config_reg_pdo_entry(sc, 0x6000, 0x11, domain, NULL)) < 0)
  {
    return (-1);
  }


  unsigned int bitposition;

  if ((var->o_din  = ecrt_slave_config_reg_pdo_entry(sc, 0x6010, 0x0c, domain, &bitposition)) < 0)
  {
    return (-1);
  }

  printf("din %d, bitpos %d\n", var->o_din, bitposition);

  var->initstate = initial;
  var->movestate = move_idle;

  if (ret == 0)
  {
//    uint8_t data;
    ret += ecrt_slave_config_sdo16(sc, 0x8010, 0x01, 4500); // Maximal current
    ret += ecrt_slave_config_sdo16(sc, 0x8010, 0x02, 4500); // Reduced current
    ret += ecrt_slave_config_sdo16(sc, 0x8010, 0x03, 48000); // "Nominal voltage"
    ret += ecrt_slave_config_sdo16(sc, 0x8010, 0x04, 28); // "Motor coil resistance"
    ret += ecrt_slave_config_sdo16(sc, 0x8010, 0x05, 0); // "Motor EMF"
    ret += ecrt_slave_config_sdo16(sc, 0x8010, 0x06, 200); // "Motor fullsteps"
    ret += ecrt_slave_config_sdo16(sc, 0x8010, 0x07, 1024); // "Encoder increments (4-fold)"
    /*
        data = 0;                                                // "Invert digital input 1"
        ret += ecrt_slave_config_sdo(  sc, 0x8012, 0x30, &data, 1);

        data = 1;                                                // "Invert digital input 2"
        ret += ecrt_slave_config_sdo(  sc, 0x8012, 0x31, &data, 1);

        data = 2;                                                // "Function for input 1"
        ret += ecrt_slave_config_sdo(  sc, 0x8012, 0x32, &data, 4);

        data = 0;                                                // "Function for input 2"
        ret += ecrt_slave_config_sdo(  sc, 0x8012, 0x36, &data, 4);

        data = 0;                                                // Feedback type
        ret += ecrt_slave_config_sdo(  sc, 0x8012, 0x08, &data, 1);

        data = 1;                                                // Operation mode
        ret += ecrt_slave_config_sdo(  sc, 0x8012, 0x01, &data, 4);

        data = 1;                                                // Speed Range
        ret += ecrt_slave_config_sdo(  sc, 0x8012, 0x05, &data, 3);


        data = 6;                                                // Microstepping
        ret += ecrt_slave_config_sdo(  sc, 0x8012, 0x45, &data, 4);

    */

    /*

    ecrt_master_sdo_download(
          system.master,
          module->infos.position,
          sdoConfig.infos.index,
          sdoConfig.infos.subIndex,
          EcTools::extractDataFromECvalue(sdoConfig.infos.value),
          sdoConfig.infos.size,
          &abort_code
        );




    {SDO{"InvertEncoderPolarity", 0x8000, 0x0E, sizeof(bool), bool(true), PhiMotorController}},



    */

    ret = ecrt_slave_config_sdo16(sc, 0x8020, 0x01, 100); // Velocity min.
    ret = ecrt_slave_config_sdo16(sc, 0x8020, 0x02, 6000); // Velocity max.

    ret = ecrt_slave_config_sdo16(sc, 0x8020, 0x03, 2000); // Acceleration pos.
    ret = ecrt_slave_config_sdo16(sc, 0x8020, 0x04, 2000); // Acceleration neg.
    ret = ecrt_slave_config_sdo16(sc, 0x8020, 0x05, 2000); // Deceleration pos.
    ret = ecrt_slave_config_sdo16(sc, 0x8020, 0x06, 2000); // Deceleration neg.
    ret = ecrt_slave_config_sdo16(sc, 0x8020, 0x07, 100); // Emergency deceleration

    ret = ecrt_slave_config_sdo32(sc, 0x8020, 0x08, 0); // Calibration position

    ret = ecrt_slave_config_sdo16(sc, 0x8020, 0x09, 1000); // Calibration velocity (towards plc cam)
    ret = ecrt_slave_config_sdo16(sc, 0x8020, 0x0a, 500); // Calibration Velocity (off plc cam)

    ret = ecrt_slave_config_sdo16(sc, 0x8020, 0x0b, 3); // Target window
    ret = ecrt_slave_config_sdo16(sc, 0x8020, 0x0c, 3000); // In-Target timeout

    ret = ecrt_slave_config_sdo16(sc, 0x8020, 0x0d, 50); // Dead time compensation
    ret = ecrt_slave_config_sdo16(sc, 0x8020, 0x10, 5000); // Position lag max.

    ret = ecrt_slave_config_sdo32(sc, 0x8020, 0x0e, 0); // Modulo factor
    ret = ecrt_slave_config_sdo32(sc, 0x8020, 0x0f, 0); // Modulo tolerance window

    /*

    data = 1;                                                // Invert calibration cam search direction
    ret += ecrt_slave_config_sdo(  sc, 0x8021, 0x13, &data, 1);

    data = 0;                                                // Invert sync impulse search direction
    ret += ecrt_slave_config_sdo(  sc, 0x8021, 0x14, &data, 1);

    data = 0;                                                // Emergency stop on position lag error
    ret += ecrt_slave_config_sdo(  sc, 0x8021, 0x15, &data, 1);

    data = 1;                                                // Enhanced diag history
    ret += ecrt_slave_config_sdo(  sc, 0x8021, 0x16, &data, 1);
    */

    /*
      0x8013:01, rwrwrw, uint16, 16 bit, "Kp factor (velo./pos.)"  : 0x03e8 1000

      0x8013:02, rwrwrw, uint16, 16 bit, "Ki factor (velo./pos.)"  : 0x0000 0

      0x8013:03, rwrwrw, uint8, 8 bit, "Inner window (velo./pos.)"  : 0x00 0

      0x8013:04, rwrwrw, type 0000, 0 bit, "SubIndex 004"  : 0x00 0

      0x8013:05, rwrwrw, uint8, 8 bit, "Outer window (velo./pos.)"  : 0x00 0

      0x8013:06, rwrwrw, uint16, 16 bit, "Filter cut off frequency (velo./pos.)"  : 0x0000 0

      0x8013:07, rwrwrw, uint16, 16 bit, "Ka factor (velo./pos.)"  : 0x0000 0

      0x8013:08, rwrwrw, uint16, 16 bit, "Kd factor (velo./pos.)"  : 0x0000 0

      0x8012:30, rwrwrw, bool, 1 bit, "Invert digital input 1"  : 0x00 0

      0x8012:31, rwrwrw, bool, 1 bit, "Invert digital input 2"  : 0x01 1

      0x8012:32, rwrwrw, type 080a, 4 bit, "Function for input 1"  : 0x01 1

      0x8012:33, rwrwrw, type 0000, 0 bit, "SubIndex 051"  : 0x01 1

      0x8012:34, rwrwrw, type 0000, 0 bit, "SubIndex 052"  : 0x01 1

      0x8012:35, rwrwrw, type 0000, 0 bit, "SubIndex 053"  : 0x01 1

      0x8012:36, rwrwrw, type 080b, 4 bit, "Function for input 2"  : 0x01 1

      0x8012:08, rwrwrw, type 0805, 1 bit, "Feedback type"  : 0x01 1
        0x8012:01, rwrwrw, type 0802, 4 bit, "Operation mode"  : 0x36 54
     0x8011:00, r-r-r-, uint8, 8 bit, "SubIndex 000"  : 0x08 8

      0x8011:01, rwrwrw, uint16, 16 bit, "Kp factor (curr.)"  : 0x0190 400

      0x8011:02, rwrwrw, uint16, 16 bit, "Ki factor (curr.)"  : 0x0004 4

      0x8011:03, rwrwrw, uint8, 8 bit, "Inner window (curr.)"  : 0x00 0

      0x8011:04, rwrwrw, type 0000, 0 bit, "SubIndex 004"  : 0x00 0

      0x8011:05, rwrwrw, uint8, 8 bit, "Outer window (curr.)"  : 0x00 0

      0x8011:06, rwrwrw, uint16, 16 bit, "Filter cut off frequency (curr.)"  : 0x0000 0

      0x8011:07, rwrwrw, uint16, 16 bit, "Ka factor (curr.)"  : 0x0064 100

      0x8011:08, rwrwrw, uint16, 16 bit, "Kd factor (curr.)"  : 0x0064 100

      0x8010:08, rwrwrw, type 0000, 16 bit, "SubIndex 008"
      0x8010:09, rwrwrw, uint16, 16 bit, "Start velocity"  : 0x0064 100

      0x8010:0a, rwrwrw, type 0000, 0 bit, "SubIndex 010"  : 0x0064 100

      0x8010:0b, rwrwrw, type 0000, 0 bit, "SubIndex 011"  : 0x0064 100

      0x8010:0c, rwrwrw, type 0000, 0 bit, "SubIndex 012"  : 0x0064 100

      0x8010:0d, rwrwrw, type 0000, 0 bit, "SubIndex 013"  : 0x0064 100

      0x8010:0e, rwrwrw, type 0000, 0 bit, "SubIndex 014"  : 0x0064 100

      0x8010:0f, rwrwrw, type 0000, 0 bit, "SubIndex 015"  : 0x0064 100

      0x8010:10, rwrwrw, uint16, 16 bit, "Drive on delay time"  : 0x0064 100

      0x8010:11, rwrwrw, uint16, 16 bit, "Drive off delay time"  : 0x0096 150

     0x8000:00, r-r-r-, uint8, 8 bit, "SubIndex 000"  : 0x0e 14

      0x8000:01, rwrwrw, type 0000, 1 bit, "SubIndex 001"
      0x8000:02, rwrwrw, type 0000, 1 bit, "SubIndex 002"
      0x8000:03, rwrwrw, type 0000, 1 bit, "SubIndex 003"
      0x8000:04, rwrwrw, type 0000, 2 bit, "SubIndex 004"
      0x8000:05, rwrwrw, type 0000, 0 bit, "SubIndex 005"
      0x8000:06, rwrwrw, type 0000, 2 bit, "SubIndex 006"
      0x8000:07, rwrwrw, type 0000, 0 bit, "SubIndex 007"
      0x8000:08, rwrwrw, bool, 1 bit, "Disable filter"  : 0x00 0

      0x8000:09, rwrwrw, type 0000, 1 bit, "SubIndex 009"
      0x8000:0a, rwrwrw, bool, 1 bit, "Enable micro increments"  : 0x00 0

      0x8000:0b, rwrwrw, type 0000, 1 bit, "SubIndex 011"
      0x8000:0c, rwrwrw, type 0000, 1 bit, "SubIndex 012"
      0x8000:0d, rwrwrw, type 0000, 1 bit, "SubIndex 013"
      0x8000:0e, rwrwrw, bool, 1 bit, "Reversion of rotation"  : 0x01 1

      0x8000:0f, rwrwrw, type 0000, 1 bit, "SubIndex 015"
      0x8000:10, rwrwrw, type 0000, 1 bit, "SubIndex 016"
      0x8000:11, rwrwrw, type 0000, 16 bit, "SubIndex 017"
      0x8000:12, rwrwrw, type 0000, 32 bit, "SubIndex 018"
    */

  }

  return (ret);
}


int EL7041readvar (uint8_t *domain_pd, EL7041vars_t *var)
{
  var->status     = EC_READ_U8 (domain_pd + var->o_status);
  var->actpos     = EC_READ_S32(domain_pd + var->o_actpos);
  var->actvel     = EC_READ_S16(domain_pd + var->o_actvel);
  var->actdrive   = EC_READ_S32(domain_pd + var->o_actdrive);
  var->intpos     = EC_READ_S32(domain_pd + var->o_intpos);
  var->moveactive = EC_READ_U16 (domain_pd + var->o_moveactive);
  var->encpos     = EC_READ_S32 (domain_pd + var->o_encpos);
  var->din        = EC_READ_U8 (domain_pd + var->o_din);
  return 0;
}



int EK1101config (int nmaster, int nslave, ec_master_t *master)
{
  ec_slave_config_t *sc;

  sc = ecrt_master_slave_config(master, nmaster, nslave, Beckhoff_EK1101);
  if (!sc)
  {
    return -1;
  }
  return 0;
}





struct timespec timespec_add(struct timespec time1, struct timespec time2)
{
  struct timespec result;

  if ((time1.tv_nsec + time2.tv_nsec) >= NSEC_PER_SEC)
  {
    result.tv_sec = time1.tv_sec + time2.tv_sec + 1;
    result.tv_nsec = time1.tv_nsec + time2.tv_nsec - NSEC_PER_SEC;
  }
  else
  {
    result.tv_sec = time1.tv_sec + time2.tv_sec;
    result.tv_nsec = time1.tv_nsec + time2.tv_nsec;
  }

  return result;
}


