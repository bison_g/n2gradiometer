#include "PID.h"


void PIDController_Init(PIDController_t *pid) {
	
	/*Clear controller variables */
	
	pid->integrator = 0.0f;
	pid->prevError = 0.0f;
	
	pid->differentiator = 0.0f; 
	pid->prevMeasurement = 0.0f; 
	
	pid->out = 0.0f; 
	
	// set constants for the Update function 

	pid->limMax =  437500./19.;//limit the output to 7V
	pid->limMin =  -437500./19.; 
	pid->setpoint = 0.;
	pid->prevout =0.0f;
	
	
	//printf("PID %i has been initialised\n", i);
}


float PIDController_Update(PIDController_t *pid, float measurement, float Kp, float Ki, float Kd, float tau, float T) {
	
	
	// implementation taken from https://www.youtube.com/watch?v=zOByx3Izf5U&t=1041s
	
	/*Error signal*/
	float error = pid->setpoint - measurement;
	
	
	/*compute the output*/
	pid->out = Kp*((1.0+T/Ki+Kd/T)*error -(1.0+2.0*Kd/T)*(pid->prevError) +(Kd/T)*(pid->prevError2)) + pid->prevout;
	//pid->out = Kp*((1.0+T/Ki)*error) +pid->prevout;
	

	
	
	if (pid->out > pid->limMax) {
		//printf("you're over the limit \n");
		
		pid->out = pid->limMax;
		
	} else if (pid->out < pid->limMin) {
		//printf("you're under the limit\n");
		pid->out = pid->limMin;
		
	}
	
	
	/*store new values*/
	pid->prevError2 = pid->prevError;
	pid->prevError = error;
	pid->prevout = pid->out;
	
	/*return output*/
	return pid->out;
}
