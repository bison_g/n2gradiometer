#ifndef FIR_FILTER_LIB_H
#define FIR_FILTER_LIB_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <malloc.h>
#include <unistd.h>
#include <string.h>
#include <new>

struct fir_filter_status_t
{
  double  *filter;
  double  *inputbuffer;
  int64_t inputbufferpos;
  int64_t inputbufferlen;
  int64_t filterlen;
  int64_t downsamplingfactor;
  int64_t inputlen;
  int64_t outputlen;
  int64_t inputcount;
  int64_t outputcount;
};


struct fir_filter_status_multistage_t
{
  int64_t inputlen;
  int64_t outputlen;
  int64_t inputcount;
  int64_t outputcount;
  int64_t bufferlen;
  int64_t filterstages;
  double  *buffer[2];
  fir_filter_status_t *statusvector;
};


int FIRinit02 (fir_filter_status_t *fs);

int FIRinit (fir_filter_status_t *fs, double *filter, long filterlen, long DSF);

void FIRdelete (fir_filter_status_t *fs);

long FIRdownsample (double *inputvector, double *outputvector, fir_filter_status_t *fs);



int FIRinitMultistage (fir_filter_status_multistage_t *fs, int64_t bufferlen);

int FIRinitMultistage_DSF10 (fir_filter_status_multistage_t *fs, int64_t bufferlen);


int FIRinitMultistage_DSF5000 (fir_filter_status_multistage_t *fs, int64_t bufferlen);
int FIRinitMultistage_DSF2000 (fir_filter_status_multistage_t *fs, int64_t bufferlen);
int FIRinitMultistage_DSF1000 (fir_filter_status_multistage_t *fs, int64_t bufferlen);
int FIRinitMultistage_DSF500 (fir_filter_status_multistage_t *fs, int64_t bufferlen);
int FIRinitMultistage_DSF200 (fir_filter_status_multistage_t *fs, int64_t bufferlen);
int FIRinitMultistage_DSF100 (fir_filter_status_multistage_t *fs, int64_t bufferlen);
int FIRinitMultistage_DSF50 (fir_filter_status_multistage_t *fs, int64_t bufferlen);
int FIRinitMultistage_DSF25 (fir_filter_status_multistage_t *fs, int64_t bufferlen);



void FIRdeleteMultistage (fir_filter_status_multistage_t *fs);

long FIRdownsampleMultistage (double *inputvector, double *outputvector, fir_filter_status_multistage_t *fs);




#endif
