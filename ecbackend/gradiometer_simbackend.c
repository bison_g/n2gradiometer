/*****************************************************************************
 *
 *  $Id$
 *
 *  Copyright (C) 2007-2009  Florian Pose, Ingenieurgemeinschaft IgH
 *
 *  This file is part of the IgH EtherCAT Master.
 *
 *  The IgH EtherCAT Master is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License version 2, as
 *  published by the Free Software Foundation.
 *
 *  The IgH EtherCAT Master is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 *  Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with the IgH EtherCAT Master; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *  ---
 *
 *  The license mentioned above concerns the source code only. Using the
 *  EtherCAT technology and brand is only permitted in compliance with the
 *  industrial property and similar rights of Beckhoff Automation GmbH.
 *
 ****************************************************************************/

#include <errno.h>
#include <math.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <sys/mman.h>
#include <malloc.h>
#include <sched.h> /* sched_setscheduler() */
#include "PID.h"
#include "PID.c"


/****************************************************************************/

#include "gradiometer_ipc.h"
#include "ethercat_config.h"

/****************************************************************************/

// Application parameters

#define CLOCK_TO_USE CLOCK_MONOTONIC

#define ETHERCAT_EPOCH_OFFSET 946684800  // output of date -u -d '1/1/2000' +%s

/****************************************************************************/

#define NSEC_PER_SEC (1000000000L)
#define PERIOD_NS (NSEC_PER_SEC / FREQUENCY)

#define DIFF_NS(A, B) (((B).tv_sec - (A).tv_sec) * NSEC_PER_SEC + \
    (B).tv_nsec - (A).tv_nsec)

#define TIMESPEC2NS(T) ((uint64_t) (T).tv_sec * NSEC_PER_SEC + (T).tv_nsec)

/****************************************************************************/

// EtherCAT

/****************************************************************************/




struct workdata_t
{
  FILE* log;

  ec_master_t *master = NULL;
  ec_master_state_t master_state;

  ec_domain_t *domain1 = NULL;
  ec_domain_state_t domain1_state;

  uint8_t *domain1_pd = NULL;

  unsigned int sync_ref_counter = 0;
  struct timespec cycletime = {0, PERIOD_NS};

  unsigned int counter = 0;

  int off_cw;
  int off_tv;
  int off_sw;
  int off_pos;
  int off_av;

  int off_lt;
  int off_le;



  // offsets for PDO entries
  int off_analog_in1[100];

// static int off_analog_out;

  int off_num_ai1;


  int off_status_ai;

  int off_time_low;
  int off_time_high;

  char filename[MAX_LEN];

  FILE *outfile;

  ipc_workdata_t *ipc;


  EL4134vars_t aout1;
  EL4134vars_t aout2;

  EL10X4vars_t din;
  ELM3148vars_t ain;
};





void cyclic_task(workdata_t *wp, float Kp[6], float Ki[6],  float Kd[6], float tau[6], float T[6])
//void cyclic_task(workdata_t *wp, float Kp[6], float Ki[6],  float Kd[6], float tau[6], float T[6], FILE *fpnt)
//void cyclic_task(workdata_t *wp,  float Kd[6], float tau[6], float T[6])
{


  feedback_data_t* fbp = &wp->ipc->fbd;

  struct timespec wakeupTime, time, Time0;



  int32_t   motorposition = 3; 
  int32_t   velocity = 0;
  int64_t scounter = 0;
  int32_t tv = 0;


  int32_t maxspeed = 1000000;
  int a = maxspeed / 100;
  int32_t goal = 0;
//  int32_t val;


  int movestate=0;

  double kp = 0.2;
  double ti = 0.1;
  double posdiff = 0;
//  double oldposdiff = 0;

//  double  valsum = 0.;
//  double  possum = 0.;

  sampledata_t* currentsample;
  sampledata_t *samplebuffer = &wp->ipc->map->samplebuffer[0];


  double read[6] = {}; //initialise input and output arrays
  double out[6] = {};




//  double decelerationdistance;

//  double ch[6];
  PIDController_t pid[6];

  for(int i=0; i<6; i++){
    PIDController_Init(&pid[i]); //Initialise all PID controllers
  }

//  uint16_t cw = 0, sw;

  // get current time
  clock_gettime(CLOCK_TO_USE, &Time0);
  clock_gettime(CLOCK_TO_USE, &wakeupTime);
  clock_gettime(CLOCK_REALTIME, &time);


//  offset = 0;
//double move = 0.;
//double move0 = 2097152./8.;

  uint64_t counter = 0;

  //variables needed for overnight measurement
//  int Vici_counter = 0;
//  char* c_time_string;



  int64_t ix;
  fbp->loopcounter = 0;
  fbp->samplecounter = 0;

  while(1)
  {
    wakeupTime = timespec_add(wakeupTime, wp->cycletime);
    clock_nanosleep(CLOCK_TO_USE, TIMER_ABSTIME, &wakeupTime, NULL);
    counter++;


    IPC_read_cfd(wp->ipc);



    if (wp->counter)
    {
      wp->counter--;
    }
    else     // do this at 1 Hz
    {
      wp->counter = (FREQUENCY/10)-1;
      scounter++;
      // check for master state (optional)
      //check_master_state();

      printf("%ld, m: %d, g: %f, v:%f, ms:%d, sc:%ld ",scounter, motorposition, wp->ipc->cfd.goalposition,wp->ipc->cfd.maxvel,movestate,fbp->samplecounter);



      printf("\n");



    }

     tv = velocity = (double) wp->ipc->cfd.maxvel/30.;
     goal = wp->ipc->cfd.goalposition;
     

     if(fabs(wp->ipc->cfd.goalposition - motorposition) < velocity || velocity <1) 
     {
       movestate=0;
       motorposition = goal;
     }
     else
     {
       movestate=1;
       if (goal > motorposition)
        {
          motorposition += velocity;
        }
        else
        {
          motorposition -= velocity;
        }

    }

    ix = (fbp->samplecounter % RINGBUFFERSIZE);
	  currentsample = &samplebuffer[ix];

    currentsample->timestamp = TIMESPEC2NS(wakeupTime);
		
    currentsample->values[0] = (double) motorposition;
    currentsample->values[1] = (double) fbp->samplecounter;
    currentsample->values[2] = (double) -1*fbp->samplecounter;
    
    fbp->correction_voltage_coil2 = currentsample->values[1];
    fbp->correction_voltage_coil4 = currentsample->values[2];
    
    fbp->samplecounter++;          




   //*******************************************************************************************
  //****   UPDATE PID CONTROLLER AND SEND OUTPUT
  //*******************************************************************************************



  for(int i=0; i<6; i++) //loop over all PID controllers
  {
      read[i] = wp -> ain.val[i][0]; //read in phases
      out[i] = PIDController_Update(&pid[i], read[i], Kp[i], Ki[i], Kd[i], tau[i], T[i]);
      //out[i] = PIDController_Update(&pid[i], read[i], wp->ipc->cfd.kp_p[i], wp->ipc->cfd.ki_p[i], Kd[i], tau[i], T[i]); // call PID controller function
      //printf("Kp of channel %i is %f\n",i, wp->ipc->cfd.ki_p[i]);
  }

  fbp->correction_voltage_coil2 = out[1]/0.00256*-1.216* 10. / 2. / 7812500.;
  fbp->correction_voltage_coil4 = out[3]/0.00256*-1.216* 10. / 2. / 7812500.;

  fbp->read_in_one = read[0];
  fbp->read_in_two = read[1];
  fbp->read_in_four = read[3];


  for(int i = 0; i<6; i++) // loop over all outputs
  {
      if(i<4) // the DACs only have four channels each
      {
        wp->aout1.val[i] = out[i]; //output channels 0-3 are used on DAC 1
        //printf("%i \n", i);
      }

      else if(i>=4 && i<6) //output channels 0 and 1 are used on DAC 2
      {
        wp->aout2.val[i-4] = out[i];
      }

      else if(i == 6)
      {
          break;
      }

  }


  /*
  //Check if the conversion0 values are correct. Produces a 3V output in Channel 2
  wp->aout1.val[3] = 6.7*0.00256/-1.216/ 10. * 2. * 7812500.;
  printf("the correcton voltage is %f \n",  wp->aout1.val[3]/0.00256*-1.216*10. / 2. / 7812500.);
  */


//*******************************************************************************************
//****   print corrected trim coil outputs (in V) into file
//*******************************************************************************************


     /*if(initstate == ready){
        //if (Vici_counter%1000 == 0){
            //printf("You got this! \n");
            //fprintf(fpnt, "You got this! \n");
            fprintf(fpnt, "%f,", read[1]/0.00256*-1.216* 10. / 2. / 7812500.);
            fprintf(fpnt,"%f \n", read[3]/0.00256*-1.216* 10. / 2. / 7812500.);
        //}
    }*/



    // write application time to master


    wp->ipc->fbd.encoderposition = motorposition;
    wp->ipc->fbd.motorposition = motorposition;
//    wp->ipc->fbd.initstate = initstate;
    wp->ipc->fbd.movestate = movestate;
    wp->ipc->fbd.targetvelocity = tv;
    wp->ipc->fbd.velocity = velocity;
    wp->ipc->fbd.posdiff = posdiff;
    wp->ipc->fbd.goalposition = goal;
    wp->ipc->fbd.kp = kp;
    wp->ipc->fbd.maxvel = maxspeed;
    wp->ipc->fbd.ti = ti;
    wp->ipc->fbd.posthreshold = 0;
    wp->ipc->fbd.a = a;
    
    fbp->loopcounter++;
    IPC_write_fbd(wp->ipc);

  }
}

/****************************************************************************/

int main(int argc, char **argv)
{
  workdata_t workdata;
  workdata_t *wp = &workdata;
  //printf("I am ignoring everything you are saying");

   //*******************************************************************************************
  //****   introduce file to print stuff into
  //*******************************************************************************************


  /*char fname[20];

  printf("Enter file name:");

  scanf("%s",fname);

  FILE* fpnt;

  fpnt = fopen(fname, "w");

  if(fpnt == NULL)
  {
    printf("Something went wrong with the file");
    }

  fprintf(fpnt, "Position 2,Position 4 \n");

  //fprintf(fpnt, "In1");
  */



  //*******************************************************************************************
  //****   configure PID CONTROLLER
  //*******************************************************************************************

//  PIDController pid1,pid2,pid3,pid4,pid5,pid6;
//  PIDController pid[6] = {pid1,pid2,pid3,pid4,pid5,pid6}; //Initialise pointers for every PIDcontroller


  //CHANGE THESE CONSTANT

  float Kp[6]={0,0.000007,0,0.000011,0,0}; //Kp[6]={0,-0.000007,0,-0.000011,0,0};  for inverted field Kp[6]={0,0.0000009,0,0.0000015,0,0};
  //printf("The size of KP is %u", sizeof(Kp));


  /*0
   * Searched for oscillation point (with Ki & Kd = 0), then reduced the parameter value by factor ten. Then incremented the value by 0.0001 steps to find optimum
   */

  float Ki[6]={0,0.0011,0,0.0012,0,0}; //Ki[6]={0,0.0011,0,0.0012,0,0};


  /*
   * after optimising Kp, introduce Ki to regulate the speed of PIC Controller approaching the setpoint.
   */




  float Kd[6]={0,pow(10,-7),0,pow(10,-7),0,0}; //Kd[6]={0,pow(10,-7), 0,0,0,0};

  /*
   * Optimised Kd value for
   *  cell 3 by regulating spead of PID Controller and then set the same for all cells.
   */




  float tau[6]={0,0.001,0.001,0.001,0.001,0.001}; //tau[6]={0,10, 0,0,0,0};
  /*
   *   Optimised tau value for cell 3 and then set the same for all cells.
   */



  float T[6]={0,0.001,0.001,0.001,0.001,0.001}; //T[6]={0,0.0000000000001, 0,0,0,0};

  /*
   *   Optimised T value for cell 3 and then set the same for all cells.
   */


  wp->sync_ref_counter = 0;
  wp->cycletime = {0, PERIOD_NS};
  wp->counter = 0;
  wp->filename[0] = 0;
  wp->outfile = NULL;



  ipc_workdata_t ipcworkdata;
  ipc_workdata_t *pwd = &ipcworkdata;


  if ( IPC_init (pwd, true) )
  {
      printf("I go in here");
    pwd->cfd.goalposition = 0;
    pwd->cfd.kp = 1;
    pwd->cfd.ti = 0.1;
    pwd->cfd.maxvel = 100000;
    pwd->cfd.tacc = 5;
    pwd->cfd.posthreshold = 5000;
    pwd->cfd.trigger = 0;

    /*
     *
     * THIS COPYING METHOD DID NOT WORK
    memcpy(&(pwd->cfd.kp_p), Kp ,sizeof(Kp));   //copy Kp and Ki to config data
    memcpy(&(pwd->cfd.ki_p), Ki , sizeof(Kp));
    */
    //memcpy(Ki, &(pwd->cfd.ki_p),sizeof(Ki));


    //THE FOLLOWING LINES OF CODE WORK FOR COPYING INTO THE CONFIG FILE


      for (unsigned int i = 0; i < (sizeof(Kp)/sizeof(Kp[0])); i++){
        pwd->cfd.kp_p[i] = Kp[i]; //copy Kp and Ki to config data
        pwd->cfd.ki_p[i] = Ki[i];

        //printf("The value of entry %i is %lf\n", i, pwd->cfd.kp_p[i]);
        //printf("The value of entry %i is %lf\n", i, pwd->cfd.ki_p[i]);
        }

    IPC_write_cfd(pwd);
  } else {
      IPC_read_cfd(pwd);
      }

  wp->ipc = pwd;




  printf("Starting cyclic function.\n");
  //cyclic_task(wp, Kd, tau, T);
  //cyclic_task(wp, Kp, Ki, Kd, tau, T, fpnt);
  cyclic_task(wp, Kp, Ki, Kd, tau, T);



  return 0;
}

/****************************************************************************/
