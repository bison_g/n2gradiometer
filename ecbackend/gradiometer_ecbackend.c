/*****************************************************************************
 *
 *  $Id$
 * 
 *  Copyright (C) 2007-2009  Florian Pose, Ingenieurgemeinschaft IgH
 *
 *  This file is part of the IgH EtherCAT Master.
 *
 *  The IgH EtherCAT Master is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License version 2, as
 *  published by the Free Software Foundation.
 *
 *  The IgH EtherCAT Master is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 *  Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with the IgH EtherCAT Master; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *  ---
 *
 *  The license mentioned above concerns the source code only. Using the
 *  EtherCAT technology and brand is only permitted in compliance with the 
 *  industrial property and similar rights of Beckhoff Automation GmbH. 
 *
 ****************************************************************************/

#include <errno.h>
#include <math.h> 
#include <signal.h>
#include <stdio.h> 
#include <string.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>     
#include <sys/mman.h>
#include <malloc.h>
#include <sched.h> /* sched_setscheduler() */
//#include "PID.h"
//#include "PID.c"

#include "firfilterlib.h"


  
/****************************************************************************/

#include "ecrt.h"

#include "gradiometer_ipc.h"
#include "ethercat_config.h"
   
/****************************************************************************/



#define FILTERLEN_DSF100_5_grad 193

double filter_DSF100_5_grad[FILTERLEN_DSF100_5_grad] = {-0.00024231771953494349,-0.0001201917843348145,-0.00013838472900716114,-0.00014887506203930482,-0.00014890416051123196,-0.0001361480293143192,-0.00010898075573219628,-0.00006676882911470314,-0.00001010652264288054,  0.0000590183382375032,0.00013721983998774435,0.00021974954152302,0.0003006830535343605,0.000373223373239113,0.00043009328399498583,0.0004640674855668145,0.00046863881018688845,0.0004387371212849706,0.0003712792244546601,0.00026567617363653945,0.00012410257796359545,-0.00004814170732967034, -0.00024254523731378087, -0.0004474498944990555, -0.0006490847039700362,-0.0008322324666126604,-0.000980911807149312,-0.001078775328898711,-0.0011129314003959327,-0.0010719035220409737,-0.0009492134507178545,-0.0007435637316887166,-0.0004593122745225461,-0.00010744740836538067,0.00029511310381368904,
   0.0007255232099232546,0.001156499937807985,0.0015573418512478386,0.0018962304023032706,0.0021420871147464406,0.002267201441174944,0.002249242028658333,0.0020737401959105752,0.0017356884858387763,0.0012411831541080847,0.0006078802615058837,-0.00013489739915420376,
   -0.0009472653975241803,-0.0017802025243805614,-0.0025782940005497947,-0.003282908906184173,-0.0038360509127167873,-0.004184126581333786,-0.004282404828630821,-0.004098968978524757,-0.003617733816396864,-0.0028415070840032376,-0.00179345396793122,-0.0005172656203503686,
   0.0009233560310117177,0.002447596978600529,0.003960839603112204,0.005360070844150915,0.006539421400886557,0.007397288294347925,0.007842996913125995,0.007804057985306501,0.007232242772862002,0.006109297289866818,0.0044506387916827105,0.002307978420420259,
   -0.00023068500489812661,-0.003042677890882874,-0.005974941935521726,-0.008849810715176532,-0.011473299108921302,-0.013644320852285213,-0.015165346324486979,-0.015853067936424638,-0.015549604877073669,-0.014132260057122661,-0.011522481881401172,-0.007692589226562533,
   -0.002670388031055429,0.0034593197710587596,0.01055545475518995,0.018425166556008857,0.02683038828143963,0.035497609370809466,0.04412909113209766,0.05241637071889731,0.060053865493940595,0.06675336981069523,0.0722573399957307,0.07635143696051808,0.0788745341113822,
   0.07972686340712293,0.0788745341113822,0.07635143696051808,0.0722573399957307,0.06675336981069523,0.060053865493940595,0.05241637071889731,0.04412909113209766,0.035497609370809466,0.02683038828143963,0.018425166556008857,0.01055545475518995,0.0034593197710587596,
   -0.002670388031055429,-0.007692589226562533,-0.011522481881401172,-0.014132260057122661,-0.015549604877073669,-0.015853067936424638,-0.015165346324486979,-0.013644320852285213,-0.011473299108921302,-0.008849810715176532,-0.005974941935521726,-0.003042677890882874,
   -0.00023068500489812661,0.002307978420420259,0.0044506387916827105,0.006109297289866818,0.007232242772862002,0.007804057985306501,0.007842996913125995,0.007397288294347925,0.006539421400886557,0.005360070844150915,0.003960839603112204,0.002447596978600529,
   0.0009233560310117177,-0.0005172656203503686,-0.00179345396793122,-0.0028415070840032376,-0.003617733816396864,-0.004098968978524757,-0.004282404828630821,-0.004184126581333786,-0.0038360509127167873,-0.003282908906184173,-0.0025782940005497947,-0.0017802025243805614,
   -0.0009472653975241803,-0.00013489739915420376,0.0006078802615058837,0.0012411831541080847,0.0017356884858387763,0.0020737401959105752,0.002249242028658333,0.002267201441174944,0.0021420871147464406,0.0018962304023032706,0.0015573418512478386,0.001156499937807985,
   0.0007255232099232546,0.00029511310381368904,-0.00010744740836538067,-0.0004593122745225461,-0.0007435637316887166,-0.0009492134507178545,-0.0010719035220409737,-0.0011129314003959327,-0.001078775328898711,-0.000980911807149312,-0.0008322324666126604,-0.0006490847039700362,   -0.0004474498944990555,-0.00024254523731378087,-0.00004814170732967034,0.00012410257796359545,0.00026567617363653945,0.0003712792244546601,0.0004387371212849706,0.00046863881018688845,0.0004640674855668145,0.00043009328399498583,0.000373223373239113,0.0003006830535343605,
   0.00021974954152302,0.00013721983998774435,0.0000590183382375032,-0.00001010652264288054,-0.00006676882911470314,-0.00010898075573219628,-0.0001361480293143192,-0.00014890416051123196,-0.00014887506203930482,-0.00013838472900716114,-0.0001201917843348145,
   -0.00024231771953494349};



// Application parameters

#define CLOCK_TO_USE CLOCK_MONOTONIC
#define MEASURE_TIMING
 
#define ETHERCAT_EPOCH_OFFSET 946684800  // output of date -u -d '1/1/2000' +%s

#define DOWNSAMPLING 10  

/****************************************************************************/

#define NSEC_PER_SEC (1000000000L)
#define PERIOD_NS (NSEC_PER_SEC / FREQUENCY)

#define DIFF_NS(A, B) (((B).tv_sec - (A).tv_sec) * NSEC_PER_SEC + \
	(B).tv_nsec - (A).tv_nsec)

#define TIMESPEC2NS(T) ((uint64_t) (T).tv_sec * NSEC_PER_SEC + (T).tv_nsec)

/****************************************************************************/

// EtherCAT

/****************************************************************************/

// process data

#define BusCouplerPos    0, 1
#define EL3751SlavePos   0, 4

#define Beckhoff_EK1501       0x00000002, 0x05dd2c52
#define Beckhoff_EL3751       0x00000002, 0x0ea73052
#define Beckhoff_EL7211       0x00000002, 0x1c2b3052
#define Beckhoff_EK1100       0x00000002, 0x044c2c52
#define Beckhoff_EL5021       0x00000002, 0x139d3052
#define Beckhoff_EL3751       0x00000002, 0x0ea73052


struct workdata_t
{
  FILE* log;
  
  ec_master_t *master = NULL;
  ec_master_state_t master_state;

  ec_domain_t *domain1 = NULL;
  ec_domain_state_t domain1_state;

  uint8_t *domain1_pd = NULL;

  unsigned int sync_ref_counter = 0;
  struct timespec cycletime = {0, PERIOD_NS};

  unsigned int counter = 0;

  int off_cw;
  int off_tv;
  int off_sw;
  int off_pos;
  int off_av;

  int off_lt;
  int off_le;



  // offsets for PDO entries
  int off_analog_in1[100];

// static int off_analog_out;

  int off_num_ai1;


  int off_status_ai;

  int off_time_low;
  int off_time_high;

  char filename[MAX_LEN];

  FILE *outfile;

  ipc_workdata_t *ipc;


  EL4134vars_t aout1;
  EL4134vars_t aout2;

  EL10X4vars_t din;
  ELM3148vars_t ain;
  ELM3004vars_t ain2;
};




ec_pdo_entry_info_t EL7211_pdo_entries[] =
{
  {0x7010, 0x01, 16}, /* Controlword */
  {0x7010, 0x06, 32}, /* Target velocity */
  {0x6000, 0x11, 32}, /* Position */
  {0x6010, 0x01, 16}, /* Statusword */
  {0x6010, 0x07, 32}, /* Velocity actual value */
};

ec_pdo_info_t EL7211_pdos[] =
{
  {0x1600, 1, EL7211_pdo_entries + 0}, /* DRV RxPDO-Map Controlword */
  {0x1601, 1, EL7211_pdo_entries + 1}, /* DRV RxPDO-Map Target velocity */
  {0x1a00, 1, EL7211_pdo_entries + 2}, /* FB TxPDO-Map Position */
  {0x1a01, 1, EL7211_pdo_entries + 3}, /* DRV TxPDO-Map Statusword */
  {0x1a02, 1, EL7211_pdo_entries + 4}, /* DRV TxPDO-Map Velocity actual value */
};

ec_sync_info_t EL7211_syncs[] =
{
  {0, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
  {1, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},
  {2, EC_DIR_OUTPUT, 2, EL7211_pdos + 0, EC_WD_DISABLE},
  {3, EC_DIR_INPUT, 3, EL7211_pdos + 2, EC_WD_DISABLE},
  {0xff}
};




ec_pdo_entry_info_t EL5021_pdo_entries[] =
{
  {0x7000, 0x01, 1}, /* Enable latch C */
  {0x0000, 0x00, 1}, /* Gap */
  {0x7000, 0x03, 1}, /* Set counter */
  {0x0000, 0x00, 13}, /* Gap */
  {0x7000, 0x11, 32}, /* Set counter value */
  {0x6000, 0x01, 1}, /* Latch C valid */
  {0x0000, 0x00, 1}, /* Gap */
  {0x6000, 0x03, 1}, /* Set counter done */
  {0x6001, 0x04, 1}, /* Frequency error */
  {0x6001, 0x05, 1}, /* Amplitude error */
  {0x0000, 0x00, 5}, /* Gap */
  {0x6000, 0x0b, 1}, /* Status of input C */
  {0x0000, 0x00, 2}, /* Gap */
  {0x6000, 0x0e, 1}, /* Sync error */
  {0x6000, 0x0f, 1}, /* TxPDO State */
  {0x6000, 0x10, 1}, /* TxPDO Toggle */
  {0x6000, 0x11, 32}, /* Counter value */
  {0x6000, 0x12, 32}, /* Latch value */
};

ec_pdo_info_t EL5021_pdos[] =
{
  {0x1600, 5, EL5021_pdo_entries + 0}, /* ENC RxPDO-Map Control */
  {0x1a00, 13, EL5021_pdo_entries + 5}, /* ENC TxPDO-Map Status */
};

ec_sync_info_t EL5021_syncs[] =
{
  {0, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
  {1, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},
  {2, EC_DIR_OUTPUT, 1, EL5021_pdos + 0, EC_WD_DISABLE},
  {3, EC_DIR_INPUT, 1, EL5021_pdos + 1, EC_WD_DISABLE},
  {0xff}
};


int EL3751configOS20 (ec_slave_config_t *sc)
{

  ec_pdo_entry_info_t slave_2_pdo_entries[] =
  {
    {0x6000, 0x01, 8},
    {0x6000, 0x09, 1},
    {0x6000, 0x0a, 1},
    {0x6000, 0x0b, 1},
    {0x0000, 0x00, 1}, /* Gap */
    {0x6000, 0x0d, 1},
    {0x6000, 0x0e, 1},
    {0x6000, 0x0f, 2},
    {0x0000, 0x00, 16}, /* Number of samples */
    {0x6001, 0x01, 32}, /* 1st sample */
    {0x6001, 0x02, 32}, 
    {0x6001, 0x03, 32},
    {0x6001, 0x04, 32},
    {0x6001, 0x05, 32},
    {0x6001, 0x06, 32},
    {0x6001, 0x07, 32},
    {0x6001, 0x08, 32},
    {0x6001, 0x09, 32},
    {0x6001, 0x0a, 32},
    {0x6001, 0x0b, 32},
    {0x6001, 0x0c, 32},
    {0x6001, 0x0d, 32},
    {0x6001, 0x0e, 32},
    {0x6001, 0x0f, 32},
    {0x6001, 0x10, 32},
    {0x6001, 0x11, 32},
    {0x6001, 0x12, 32},
    {0x6001, 0x13, 32},
    {0x6001, 0x14, 32},
    {0xf600, 0x01, 32}, /* PAI Timestamp low  */
    {0xf600, 0x02, 32}, /* PAI Timestamp high */
  };

  ec_pdo_info_t slave_2_pdos[] =
  {
    {0x1a00, 9, slave_2_pdo_entries + 0},
    {0x1a08, 20, slave_2_pdo_entries + 9},
    {0x1a10, 2, slave_2_pdo_entries + 29},
  };

  ec_sync_info_t slave_2_syncs[] =
  {
    {0, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
    {1, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},
    {2, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
    {3, EC_DIR_INPUT, 3, slave_2_pdos + 0, EC_WD_DISABLE},
    {0xff}
  };


 


  int ret;
  ret = ecrt_slave_config_pdos(sc, EC_END, slave_2_syncs);
  if (ret)
  {
    return (ret);
  }

#define EL3751_Interface   0x8000, 0x01
#define EL3751_SG_Voltage  0x8000, 0x02
#define EL3751_Filter1     0x8000, 0x16
#define EL3751_Filter1_FIR_Notch_50   1
#define EL3751_Filter1_FIR_Notch_60   2
#define EL3751_Filter1_FIR_LP_100     3
#define EL3751_Filter1_FIR_LP_1000    4
#define EL3751_Filter1_FIR_HP_150     5
#define EL3751_Filter1_FIR_HP_1500    6
#define EL3751_Filter1_IIR_Notch_50  16
#define EL3751_Filter1_IIR_Notch_60  17
#define EL3751_Filter1_IIR_LP_1      18
#define EL3751_Filter1_IIR_LP_25     19
#define EL3751_Filter1_IIR_LP_100    20
#define EL3751_Filter1_IIR_LP_250    21
#define EL3751_Filter1_IIR_LP_1000   22
#define EL3751_DecimationFactor  0x8000, 0x18
#define EL3751_SamplingRate  0x9000, 0x13

#define EL3751_SMOutSyncMode  0x1C32, 0x01
#define EL3751_SMOutCycleTime 0x1C32, 0x02
#define EL3751_SMInSyncMode   0x1C33, 0x01
#define EL3751_SMInCycleTime  0x1C33, 0x02

  ret = ecrt_slave_config_sdo16(sc, EL3751_Interface,  3  ); // +- 5 V
//    ret = ecrt_slave_config_sdo16(sc, EL3751_Interface,  65  ); // Poti 3-wire
//    ret = ecrt_slave_config_sdo16(sc, EL3751_SG_Voltage, 10  ); // 5V
//    ret = ecrt_slave_config_sdo16(sc, EL3751_Filter1, EL3751_Filter1_IIR_LP_25  );
//  ret = ecrt_slave_config_sdo16(sc, EL3751_Filter1, 0 );
  ret = ecrt_slave_config_sdo16(sc, EL3751_DecimationFactor, 20 );  // decimation 20 -> samplingrate 100 Hz

//  ret = ecrt_slave_config_sdo16(sc, EL3751_SMOutSyncMode, 2 );  // DC synchrouneous mode
//  ret = ecrt_slave_config_sdo16(sc, EL3751_SMInSyncMode, 2 );  // DC synchrouneous mode

  ecrt_slave_config_dc(sc, 0x700,  10000000, 0, 0, 0);

//    ret = ecrt_slave_config_sdo32(sc, EL3751_SMOutCycleTime, 2000000 );  // 2 ms
//    ret = ecrt_slave_config_sdo32(sc, EL3751_SMInCycleTime,  2000000 );  // 2 ms

  return ret;
}







/*****************************************************************************/
/*
void check_domain1_state(void)
{
  ec_domain_state_t ds;

  ecrt_domain_state(domain1, &ds);

  if (ds.working_counter != domain1_state.working_counter)
    printf("Domain1: WC %u.\n", ds.working_counter);
  if (ds.wc_state != domain1_state.wc_state)
    printf("Domain1: State %u.\n", ds.wc_state);

  domain1_state = ds;
}
*/
/*****************************************************************************/
/*
void check_master_state(void)
{
  ec_master_state_t ms;

  ecrt_master_state(master, &ms);

  if (ms.slaves_responding != master_state.slaves_responding)
    printf("%u slave(s).\n", ms.slaves_responding);
  if (ms.al_states != master_state.al_states)
    printf("AL states: 0x%02X.\n", ms.al_states);
  if (ms.link_up != master_state.link_up)
    printf("Link is %s.\n", ms.link_up ? "up" : "down");

  master_state = ms;
} 
*/
/****************************************************************************/

void cyclic_task(workdata_t *wp, float Kp[6], float Ki[6],  float Kd[6], float tau[6], float T[6])
//void cyclic_task(workdata_t *wp, float Kp[6], float Ki[6],  float Kd[6], float tau[6], float T[6], FILE *fpnt)
//void cyclic_task(workdata_t *wp,  float Kd[6], float tau[6], float T[6])
{
	
  fir_filter_status_t firstatus1;
  fir_filter_status_t firstatus2;
  fir_filter_status_t firstatus3;

  FIRinit(&firstatus1, filter_DSF100_5_grad, FILTERLEN_DSF100_5_grad, 5);
  FIRinit(&firstatus2, filter_DSF100_5_grad, FILTERLEN_DSF100_5_grad, 5);
  FIRinit(&firstatus3, filter_DSF100_5_grad, FILTERLEN_DSF100_5_grad, 5);

  firstatus1.outputlen=1;
  firstatus2.outputlen=1;
  firstatus3.outputlen=1;

  firstatus1.inputlen=1;
  firstatus2.inputlen=1;
  firstatus3.inputlen=1;





  feedback_data_t* fbp = &wp->ipc->fbd;
        
  struct timespec wakeupTime, time, Time0;

  enum initstates { initial,
                    init0,
                    init1,
                    init2,
                    init3,
                    init4,
                    ready,
                  } initstate;
  initstate = initial;

  enum movingstates
  {
    hold,
    accelerate,
    decelerate
  } movestate;
  movestate = hold;

  uint64_t  clockoffset;

// FILE *out;

  // out=fopen("out.txt","wt");
 
#ifdef MEASURE_TIMING
  struct timespec startTime, endTime, lastStartTime = {};
  uint32_t period_ns = 0, exec_ns = 0, latency_ns = 0,
           latency_min_ns = 0, latency_max_ns = 0,
           period_min_ns = 0, period_max_ns = 0,
           exec_min_ns = 0, exec_max_ns = 0;

 
#endif

  int32_t   motorposition, velocity; 
  int64_t scounter = 0;
  int32_t tv = 0;
  
  //, , lv;
//  uint8_t   lt, num1;
  
//  double avval; 
  

//  double pos, vel, projectedpos;

  int32_t maxspeed = 1000000;
  int a = maxspeed / 10;
  int32_t goal = 0;
//  int32_t val;

  double offset = 0;


  double kp = 0.2;
  double ti = 0.1;
  double posdiff = 0;
  double oldposdiff = 0;

  sampledata_t* currentsample;
  sampledata_t *samplebuffer = &wp->ipc->map->samplebuffer[0];

  int posthreshold;

 // double read[6] = {}; //initialise input and output arrays
//  double out[6] = {};

  double decelerationdistance;

//  double ch[6];
  //PIDController_t pid[6];
/*
  for(int i=0; i<6; i++){	  
	PIDController_Init(&pid[i]); //Initialise all PID controllers
  }
*/

  uint16_t cw = 0, sw;

  // get current time
  clock_gettime(CLOCK_TO_USE, &Time0);
  clock_gettime(CLOCK_TO_USE, &wakeupTime);
  clock_gettime(CLOCK_REALTIME, &time);
  clockoffset = DIFF_NS(wakeupTime, time) - ETHERCAT_EPOCH_OFFSET * NSEC_PER_SEC;


  offset = 0;
//double move = 0.;
//double move0 = 2097152./8.;

  uint64_t counter = 0;
  
  //variables needed for overnight measurement
  int Vici_counter = 0;
//  char* c_time_string;
 
  
  
  int64_t ix;
  fbp->loopcounter = 0;
  fbp->samplecounter = 0; 
  IPC_read_cfd(wp->ipc);
  kp = wp->ipc->cfd.kp;
  goal = wp->ipc->cfd.goalposition;
  maxspeed = wp->ipc->cfd.maxvel;
  posthreshold = wp->ipc->cfd.posthreshold;
  a = maxspeed / wp->ipc->cfd.tacc;
  ti = wp->ipc->cfd.ti;
  offset = wp->ipc->cfd.offset;



  while(1)
  {
    wakeupTime = timespec_add(wakeupTime, wp->cycletime);
    clock_nanosleep(CLOCK_TO_USE, TIMER_ABSTIME, &wakeupTime, NULL);
    counter++;

#ifdef MEASURE_TIMING
    clock_gettime(CLOCK_TO_USE, &startTime);
    latency_ns = DIFF_NS(wakeupTime, startTime);
    period_ns = DIFF_NS(lastStartTime, startTime);
    exec_ns = DIFF_NS(lastStartTime, endTime);
    lastStartTime = startTime;

    if (latency_ns > latency_max_ns)
    {
      latency_max_ns = latency_ns;
    }
    if (latency_ns < latency_min_ns)
    {
      latency_min_ns = latency_ns;
    }
    if (period_ns > period_max_ns)
    {
      period_max_ns = period_ns;
    }
    if (period_ns < period_min_ns)
    {
      period_min_ns = period_ns;
    }
    if (exec_ns > exec_max_ns)
    {
      exec_max_ns = exec_ns;
    }
    if (exec_ns < exec_min_ns)
    {
      exec_min_ns = exec_ns;
    }
#endif

    // receive process data
    ecrt_master_receive(wp->master);
    ecrt_domain_process(wp->domain1);
    ec_domain_state_t ds;
    ecrt_domain_state(wp->domain1, &ds);

    // check process data state (optional)
//    check_domain1_state();



    static uint8_t *domain_pd = ecrt_domain_data(wp->domain1);

    // write process data
    motorposition = EC_READ_S32(domain_pd + wp->off_pos);
    velocity = EC_READ_S32(domain_pd + wp->off_av);
    sw = EC_READ_U16(domain_pd + wp->off_sw);
    
    ELM3148readvar(domain_pd, &(wp->ain));
    ELM3004readvar(domain_pd, &(wp->ain2));

//    encoderposition = EC_READ_S32(domain_pd + wp->off_cv);  // counter value
//    lv = EC_READ_S32(domain_pd + wp->off_lv);  // latch value
//    lt = EC_READ_U8(domain_pd + wp->off_lt);

//    num1 = EC_READ_U8(domain_pd + wp->off_num_ai1);


/*
 *     for (int i = 0; i < num1 && i < 20; i++)
    {
      val = EC_READ_S32(domain_pd + wp->off_analog_in1[i]);
//                printf("%d -> %d\n",i,val);
    }
*/
    IPC_read_cfd(wp->ipc);





   //*******************************************************************************************
  //****   UPDATE PID CONTROLLER AND SEND OUTPUT
  //*******************************************************************************************
  
  
  
//  fbp->correction_voltage_coil2 = out[1]/0.00256*-1.216* 10. / 2. / 7812500.;
//  fbp->correction_voltage_coil4 = out[3]/0.00256*-1.216* 10. / 2. / 7812500.;


//  fbp->correction_voltage_coil2 = wp -> ain.val[1][0] * 10. / 7812500.;
//  fbp->correction_voltage_coil4 = wp -> ain.val[3][0] * 10. / 7812500.;

  fbp->correction_voltage_coil2 = wp->ain2.av_val[0] * wp->ain2.range / 7812500.;
  fbp->correction_voltage_coil4 = wp->ain2.av_val[1] * wp->ain2.range / 7812500.;

//    pos = position;
// vel = velocity / 256.;

    if (wp->counter)
    {
      wp->counter--;
    }
    else     // do this at 1 Hz
    {
      wp->counter = FREQUENCY/10;
      scounter++;
      // check for master state (optional)
      //check_master_state();

/*      printf("%ld ",scounter);
      for (int i=0;i<8;i++)
      {
		  printf("%d, ",wp->ain.val[i][0]);
		  printf("v:%7.3f, ",(double)wp->ain.val[i][0]/7812500.);

	  }
	  
      printf("\n");
      // ZORAN Debug 
      
      printf("volt2: %15.6f,   volt4=%15.6f\n", fbp->correction_voltage_coil2,  fbp->correction_voltage_coil4);
    
    
      for(int i=0;i<6;i++){
		//printf("Kp[%i] = %f Ki[%i] = %f ",i, Kp[i],i, Ki[i]);
		printf("Kp[%i] = %f Ki[%i] = %f ",i, wp->ipc->cfd.kp_p[i],i, wp->ipc->cfd.ki_p[i]);
	  }
	  printf("\n");
	*/  

#ifdef MEASURE_TIMING
      // output timing stats
      /*  printf("period     %10u ... %10u\n",
               period_min_ns, period_max_ns);
        printf("exec       %10u ... %10u\n",
               exec_min_ns, exec_max_ns);
        printf("latency    %10u ... %10u\n",
               latency_min_ns, latency_max_ns); */
      period_max_ns = 0;
      period_min_ns = 0xffffffff;
      exec_max_ns = 0;
      exec_min_ns = 0xffffffff;
      latency_max_ns = 0;
      latency_min_ns = 0xffffffff;

#endif

      clock_gettime(CLOCK_REALTIME, &time);

//      time64= TIMESPEC2NS(time);




      //  if (scounter % 200 ==  0)  move = 0;
      //  if (scounter % 200 ==  50)  move = -move0;
      // if (scounter % 200 == 100)  move = 0;
      // if (scounter % 200 == 150)  move =  move0;

    }


    switch(initstate)
    {
    case initial:
      cw = 0;
      if (ds.wc_state == 2)
      {
        initstate = init0;
      }
      break;
    case init0:
      cw = 0x80;                            // fault reset
      if ( (sw & 8) == 0)
      {
        initstate = init1;  // wait until fault bit is low
      }
      break;
    case init1:
      cw = 0x06;                           // enable voltage
      if ( (sw & 64) == 0)
      {
        initstate = init2;  // wait until "switch on disabled" is low
      }
      break;
    case init2:
      cw = 0x07;
      if (sw  == 35)
      {
        initstate = init3;
      }
      break;
    case init3:
      cw = 0x0F;
      if (sw  == 4135)
      {
        initstate = init4;
      }
      break;
    case init4:
      cw = 0x0F;
      if (sw == 4135)
      {
        initstate = ready;
        movestate = hold;
        printf("\n\n READY \n\n");
      }
      break;
    case ready:
      cw = 0x0F;
      if ((sw & 1) == 0)
      {
        initstate = init0;
        printf("\n\n Init0 \n\n");
      }
      break;
    }

    posdiff = offset + goal - motorposition;

    double sign = (posdiff >= 0) ? 1 : -1;
    int approachspeed = 100000;

    if (initstate == ready) // move state mashine is active only if init is ready
    {
      decelerationdistance = (double) velocity / a * velocity / 2 * 4;

      switch(movestate)
      {
      case hold:

        kp = wp->ipc->cfd.kp;
        goal = wp->ipc->cfd.goalposition;
        maxspeed = wp->ipc->cfd.maxvel;
        posthreshold = wp->ipc->cfd.posthreshold;
        a = maxspeed / wp->ipc->cfd.tacc;
        if (a<10000 )   a = 10000;
        if (a>20000000) a= 20000000;
        
        
        ti = wp->ipc->cfd.ti;
        offset = wp->ipc->cfd.offset;

        if (fabs(posdiff) < posthreshold)
        {
          tv = (double) kp * (posdiff - oldposdiff + 1 / ti * posdiff);

          if (tv > approachspeed)
          {
            tv = approachspeed;
          }
          if (tv < -approachspeed)
          {
            tv = -approachspeed;
          }

        }

        if (fabs(posdiff) > posthreshold)
        {
          movestate = accelerate;
          printf("a:%d tacc:%f\n goal:%d, mot:%d",a,wp->ipc->cfd.tacc,goal,motorposition);
        }
        break;

      case accelerate:
        if (fabs(posdiff) < decelerationdistance + 2.2 * posthreshold)
        {
          movestate = decelerate;
        }
        else
        {
          if (abs(tv) < maxspeed)
          {
            tv += (double) sign * a / FREQUENCY;
          }
        }
        break;

      case decelerate:
        if (fabs(tv) > approachspeed)
        {
          tv -= (double) sign * a / FREQUENCY;
        }
        else
        {
          movestate = hold;
        }

        break;

      }

      if (fabs(posdiff) < posthreshold)
      {
        movestate = hold;
      }

  double posin,posout;
  double val1out, val2out;
  posin = (double) motorposition;

  int fir1, fir2,fir3;

  fir1=FIRdownsample(&posin, &posout, &firstatus1);
  fir2=FIRdownsample(&fbp->correction_voltage_coil2, &val1out, &firstatus2);
  fir3=FIRdownsample(&fbp->correction_voltage_coil4, &val2out, &firstatus3);


  if (fir1 && fir2 && fir3)
  {

        ix = (fbp->samplecounter % RINGBUFFERSIZE);
  	    currentsample = &samplebuffer[ix];
        currentsample->timestamp = TIMESPEC2NS(wakeupTime);

        currentsample->values[0] = posout;
        currentsample->values[1] = val1out;
        currentsample->values[2] = val2out;

        fbp->samplecounter++;
      //  printf("%6ld, %9fd,%9d, %9f,%9f, %9f,%9f\n",fbp->samplecounter,currentsample->values[0],motorposition,currentsample->values[1],fbp->correction_voltage_coil2, currentsample->values[2],fbp->correction_voltage_coil4);

        }





    } // initstate == ready

    if (tv > maxspeed)
    {
      tv = maxspeed;
    }
    if (tv < -maxspeed)
    {
      tv = -maxspeed;
    }

    oldposdiff = posdiff;

/*
    if (counter % 100 == 0)
    {
      printf("%5ld,  posdev: %10.0f, tv: %10d, dd: %10.0f, cw: %5d, sw: %5d (%5d) %5d,  ms:%d\n",
             scounter, posdiff, tv, decelerationdistance, cw, sw, (sw & 8) == 0, initstate,  movestate);

    }

*/

    /*    if (scounter>7)
          {
              if (position>100) tv=0;
              if (position>1000) tv=-1*maxspeed/100;
              if (position>10000) tv=-1*maxspeed/10;
              if (position>1000000) tv=-1*maxspeed;
          }
    */


//   int32_t tv0=0;




//    int a = maxspeed / 1000;
//    int sign;
    /*  if (scounter>10)
      {

        goal=offset+move;

        projectedpos = (double) pos + 0.5*vel*fabs(vel)/a*256.;

        if (goal > position)
        {
          sign =+1;
        }
        else
        {
          sign=-1;
        }

        if (state == 0) // keep position state
        {
          if (abs(position-goal)>500000)
          {
            state = 1; // big deviation-> go to accelerate state
          }
          else
          {

            tv=0;
            if (abs(position-goal)>100)    tv=sign*1000;
            if (abs(position-goal)>1000)   tv=sign*4000;
            if (abs(position-goal)>5000)   tv=sign*10000;
            if (abs(position-goal)>20000)  tv=sign*40000;
          }
        }  
        *                        
        if (state==1) // accelerate state
        { 
          if  (sign*projectedpos > sign*goal - 100000)
          {
            state=2; // decelerate before we overshoot the goal
          }  
          else
          { 
       
            tv += sign*a;           
 
            if (abs(tv)>maxspeed)
            {   
              tv = sign*maxspeed;
            }

          }
        }     

        if (state==2) // decelerate state 
        {
          if  (abs(position-goal)<100000||abs(velocity)<100000)
          {     
            state=0; // go to position holding state   
          }   
          else  
          {   
    
            tv -= sign*a;     
          }       
        }        

          
        //fprintf(out,"%ld\t%d\t%d    
    */   
          
//     tv=10000000/10;  
  
  /*for(int i=0;i<4;i++) // produces ramps   
  {  
	  wp->aout1.val[i]++;     
	  wp->aout2.val[i]++;     
  }*/   
  
  
  

  
  /*
  //Check if the conversion0 values are correct. Produces a 3V output in Channel 2
  wp->aout1.val[3] = 6.7*0.00256/-1.216/ 10. * 2. * 7812500.;
  printf("the correcton voltage is %f \n",  wp->aout1.val[3]/0.00256*-1.216*10. / 2. / 7812500.);
  */

  
//*******************************************************************************************
//****   print corrected trim coil outputs (in V) into file
//*******************************************************************************************
 
 
	 /*if(initstate == ready){
		//if (Vici_counter%1000 == 0){
			//printf("You got this! \n");
			//fprintf(fpnt, "You got this! \n");
			fprintf(fpnt, "%f,", read[1]/0.00256*-1.216* 10. / 2. / 7812500.);
			fprintf(fpnt,"%f \n", read[3]/0.00256*-1.216* 10. / 2. / 7812500.);
		//}
	}*/
	


    EL4134writevar (domain_pd, &wp->aout1);
   
    EL4134writevar (domain_pd, &wp->aout2);

    EC_WRITE_S32(domain_pd + wp->off_tv, tv);
    EC_WRITE_U16(domain_pd + wp->off_cw, cw);


    // write application time to master



    clock_gettime(CLOCK_TO_USE, &time);
    ecrt_master_application_time(wp->master, clockoffset + TIMESPEC2NS(time));

    if (wp->sync_ref_counter)
    {
      wp->sync_ref_counter--;
    }
    else
    {
      wp->sync_ref_counter = 1; // sync every cycle
      ecrt_master_sync_reference_clock(wp->master);
    }
    ecrt_master_sync_slave_clocks(wp->master);

    // send process data
    ecrt_domain_queue(wp->domain1);
    ecrt_master_send(wp->master);

#ifdef MEASURE_TIMING
    clock_gettime(CLOCK_TO_USE, &endTime);
#endif

    wp->ipc->fbd.encoderposition = motorposition;
    wp->ipc->fbd.motorposition = motorposition;
    wp->ipc->fbd.initstate = initstate;
    wp->ipc->fbd.movestate = movestate;
    wp->ipc->fbd.targetvelocity = tv;
    wp->ipc->fbd.velocity = velocity;
    wp->ipc->fbd.posdiff = posdiff;
    wp->ipc->fbd.goalposition = goal;
    wp->ipc->fbd.kp = kp;
    wp->ipc->fbd.maxvel = maxspeed;
    wp->ipc->fbd.ti = ti;
    wp->ipc->fbd.posthreshold = posthreshold;
    wp->ipc->fbd.a = a;

    if ( strcmp(wp->ipc->cfd.filename, wp->filename) != 0 )
    {
      if(wp->outfile)
      {
        fclose(wp->outfile);
      }


      strncpy(wp->filename, wp->ipc->cfd.filename, MAX_LEN);
      if  (strlen(wp->filename) > 0)
      {
        wp->outfile = fopen(wp->filename, "wt");


        if (wp->outfile == NULL )
        {
          printf("open outfile failed. filename: %s\n", wp->filename);
        }
        else
        {
          printf("open filename: %s\n", wp->filename);
        }
      }

    } 

 
	Vici_counter+=1;
    fbp->loopcounter++;
    IPC_write_fbd(wp->ipc);

  } 
} 
 
/****************************************************************************/

int main(int argc, char **argv)  
{
  workdata_t workdata; 
  workdata_t *wp = &workdata; 
  //printf("I am ignoring everything you are saying");
  
   //*******************************************************************************************
  //****   introduce file to print stuff into
  //*******************************************************************************************
  
  
  /*char fname[20];
  
  printf("Enter file name:");
  
  scanf("%s",fname);
  
  FILE* fpnt; 
  
  fpnt = fopen(fname, "w");
  
  if(fpnt == NULL)
  {
	printf("Something went wrong with the file");
	}
 
  fprintf(fpnt, "Position 2,Position 4 \n"); 
  
  //fprintf(fpnt, "In1");
  */
  
  
  
  //*******************************************************************************************
  //****   configure PID CONTROLLER
  //*******************************************************************************************
  
//  PIDController pid1,pid2,pid3,pid4,pid5,pid6; 
//  PIDController pid[6] = {pid1,pid2,pid3,pid4,pid5,pid6}; //Initialise pointers for every PIDcontroller
  
   
  //CHANGE THESE CONSTANT 
  
  float Kp[6]={0,0.000007,0,0.000011,0,0}; //Kp[6]={0,-0.000007,0,-0.000011,0,0};  for inverted field Kp[6]={0,0.0000009,0,0.0000015,0,0};
  //printf("The size of KP is %u", sizeof(Kp));
  
  
  /*0
   * Searched for oscillation point (with Ki & Kd = 0), then reduced the parameter value by factor ten. Then incremented the value by 0.0001 steps to find optimum
   */

  float Ki[6]={0,0.0011,0,0.0012,0,0}; //Ki[6]={0,0.0011,0,0.0012,0,0};
  
  
  /*
   * after optimising Kp, introduce Ki to regulate the speed of PIC Controller approaching the setpoint.
   */

   
  
  
  float Kd[6]={0,pow(10,-7),0,pow(10,-7),0,0}; //Kd[6]={0,pow(10,-7), 0,0,0,0};
  
  /*
   * Optimised Kd value for
   *  cell 3 by regulating spead of PID Controller and then set the same for all cells. 
   */
  
  
  
  
  float tau[6]={0,0.001,0.001,0.001,0.001,0.001}; //tau[6]={0,10, 0,0,0,0};
  /*
   *   Optimised tau value for cell 3 and then set the same for all cells.
   */ 

  
  
  float T[6]={0,0.001,0.001,0.001,0.001,0.001}; //T[6]={0,0.0000000000001, 0,0,0,0};
  
  /*
   *   Optimised T value for cell 3 and then set the same for all cells.
   */ 
 
  

  ec_slave_config_t *sc;

  wp->sync_ref_counter = 0;
  wp->cycletime = {0, PERIOD_NS};
  wp->counter = 0;
  wp->filename[0] = 0;
  wp->outfile = NULL;

  wp->log = fopen("log.txt", "wt");

  if (wp->log == NULL )
  {
    perror("open logfile failed");
    return -1;
  }

  ipc_workdata_t ipcworkdata;
  ipc_workdata_t *pwd = &ipcworkdata;


  if ( IPC_init (pwd, true) )
  {
	  printf("I go in here");
    pwd->cfd.goalposition = 0;
    pwd->cfd.kp = 1;
    pwd->cfd.ti = 0.1;
    pwd->cfd.maxvel = 100000;
    pwd->cfd.tacc = 15;
    pwd->cfd.posthreshold = 5000;
    pwd->cfd.trigger = 0;
    pwd->cfd.offset = 0;
    
    /*
     * 
     * THIS COPYING METHOD DID NOT WORK
    memcpy(&(pwd->cfd.kp_p), Kp ,sizeof(Kp));   //copy Kp and Ki to config data
    memcpy(&(pwd->cfd.ki_p), Ki , sizeof(Kp));
    */
    //memcpy(Ki, &(pwd->cfd.ki_p),sizeof(Ki));  
    
    
    //THE FOLLOWING LINES OF CODE WORK FOR COPYING INTO THE CONFIG FILE
    
     
      for (unsigned int i = 0; i < (sizeof(Kp)/sizeof(Kp[0])); i++){
		pwd->cfd.kp_p[i] = Kp[i]; //copy Kp and Ki to config data 
		pwd->cfd.ki_p[i] = Ki[i];
		
		//printf("The value of entry %i is %lf\n", i, pwd->cfd.kp_p[i]);
		//printf("The value of entry %i is %lf\n", i, pwd->cfd.ki_p[i]);
		} 
        
    IPC_write_cfd(pwd);
  } else {
	  IPC_read_cfd(pwd);
	  }

  wp->ipc = pwd;


  if (mlockall(MCL_CURRENT | MCL_FUTURE) == -1)
  {
    perror("mlockall failed");
    return -1;
  }

  wp->master = ecrt_request_master(0);
  if (!wp->master)
  {
    return -1;
  }

  wp->domain1 = ecrt_master_create_domain(wp->master);
  if (!wp->domain1)
  {
    return -1;
  }

  // Create configuration for bus coupler
  sc = ecrt_master_slave_config(wp->master, BusCouplerPos, Beckhoff_EK1100);
  if (!sc)
  {
    return -1;
  }



  //*******************************************************************************************
  //****   configure EL7211
  //*******************************************************************************************

  if (!(sc = ecrt_master_slave_config(wp->master, 0, 3, Beckhoff_EL7211)))
  {
    fprintf(stderr, "Failed to get slave configuration.\n");
    return -1;
  }

  int ret;
  ret = ecrt_slave_config_pdos(sc, EC_END, EL7211_syncs);
  printf("ecrt_slave_config_pdos %d\n", ret);
  if (ret)
  {
    fprintf(stderr, "ecrt_slave_config_pdos failed.\n");
    return -1;
  }

  // {0x7010, 0x01, 16}, /* Controlword */
  //  {0x7010, 0x06, 32}, /* Target velocity */
  //  {0x6000, 0x11, 32}, /* Position */
  //  {0x6010, 0x01, 16}, /* Statusword */
  //  {0x6010, 0x07, 32}, /* Velocity actual value */


  ecrt_slave_config_dc(sc, 0x0700, PERIOD_NS, 4400000, 0, 0);

  if ((wp->off_cw    = ecrt_slave_config_reg_pdo_entry(sc, 0x7010, 0x01, wp->domain1, NULL)) < 0)
  {
    ret = -1;
  }
  if (ret)
  {
    fprintf(stderr, "ecrt_slave_config_reg_pdo_entry failed.\n");
  }


  if ((wp->off_tv    = ecrt_slave_config_reg_pdo_entry(sc, 0x7010, 0x06, wp->domain1, NULL)) < 0)
  {
    ret = -1;
  }
  if (ret)
  {
    fprintf(stderr, "ecrt_slave_config_reg_pdo_entry failed.\n");
  }

  if ((wp->off_pos    = ecrt_slave_config_reg_pdo_entry(sc, 0x6000, 0x11, wp->domain1, NULL)) < 0)
  {
    ret = -1;
  }
  if (ret)
  {
    fprintf(stderr, "ecrt_slave_config_reg_pdo_entry failed.\n");
  }

  if ((wp->off_av     = ecrt_slave_config_reg_pdo_entry(sc, 0x6010, 0x07, wp->domain1, NULL)) < 0)
  {
    ret = -1;
  }
  if (ret)
  {
    fprintf(stderr, "ecrt_slave_config_reg_pdo_entry failed.\n");
  }
 

  if ((wp->off_sw    = ecrt_slave_config_reg_pdo_entry(sc, 0x6010, 0x01, wp->domain1, NULL)) < 0)
  {
    ret = -1;
  }
  if (ret)
  {
    fprintf(stderr, "ecrt_slave_config_reg_pdo_entry failed.\n");
  }

  EL4134config(0, 4, wp->master, wp->domain1, &(wp->aout1));
  EL4134config(0, 5, wp->master, wp->domain1, &(wp->aout2));
  
  EL1014config(0, 6, wp->master, wp->domain1, &(wp->din));
  ELM3148config(0, 7, wp->master, wp->domain1, &(wp->ain));
  ELM3004config(0, 8, wp->master, wp->domain1, &(wp->ain2));


// int ELM3148config(int nmaster, int nslave, ec_master_t *master, ec_domain_t *domain, ELM3004vars_t *var)
//file:///run/media/daq/KINGSTON/noise_all_no_motor.txt
//file:///run/media/daq/KINGSTON/noise_all.txt


  //*******************************************************************************************
  //****   configure EL3751
  //*******************************************************************************************

/*

  if (!(sc = ecrt_master_slave_config(wp->master, EL3751SlavePos, Beckhoff_EL3751)))
  {
    fprintf(stderr, "Failed to get slave configuration.\n");
    return -1;
  }

  EL3751configOS20(sc);


  for (int i = 0; i < 20; i++)
  {
    wp->off_analog_in1[i] = ecrt_slave_config_reg_pdo_entry(sc, 0x6001, i + 1, wp->domain1, NULL);
    if (wp->off_analog_in1[i] <= 0)
    {
      return -1;
    }
  }

  wp->off_num_ai1 =   ecrt_slave_config_reg_pdo_entry(sc, 0x6000, 0x1, wp->domain1, NULL);
  if (wp->off_num_ai1 < 0)
  {
    return -1;
  }

*/




  printf("Activating master...\n");
  if (ecrt_master_activate(wp->master))
  {
    return -1;
  }

  /*  if (!(domain1_pd = ecrt_domain_data(domain1)))
    {
      return -1;
    }
  */

  /* Set priority */

  struct sched_param param = {};
  param.sched_priority = sched_get_priority_max(SCHED_FIFO);

  printf("Using priority %i.", param.sched_priority);
  if (sched_setscheduler(0, SCHED_FIFO, &param) == -1)
  {
    perror("sched_setscheduler failed");
  }
  

  printf("Starting cyclic function.\n");
  //cyclic_task(wp, Kd, tau, T);
  //cyclic_task(wp, Kp, Ki, Kd, tau, T, fpnt);
  cyclic_task(wp, Kp, Ki, Kd, tau, T);

  if(wp->outfile)
  {
    fclose(wp->outfile);
  }
  


  return 0;
}

/****************************************************************************/
