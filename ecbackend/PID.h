#ifndef PID_CONTROLLER_H
#define PID_CONTROLLER_H

typedef struct {
    

	/* Controller gains*/
	float Kp; /*Proportial term*/
	float Ki; /*integration*/
	float Kd; /*integration*/

	/*derivative low-pass filter time constant */
	float tau;

	/*Output limits*/
	float limMin; 
	float limMax;

	/*Sample time in seconds*/
	float T; 

	/*Controller "memory"*/
	float integrator;
	float prevError2;
	float prevout;
	float prevError; /*used in integrator*/
	float differentiator; 
	float prevMeasurement; /*used in differentiator, because differentiate measurement. not error */
	
	/*Controller setpoint*/
	float setpoint;

	/*Controller output*/
	float out;

} PIDController_t; 

void PIDController_Init(PIDController_t *pid); //Initialises PID parameters
float PIDController_Update(PIDController_t *pid, float setpoint, float measurement); //Updates the PID Controller output

#endif 

