#ifndef IPC_H
#define IPC_H


/***** TCP/IP port definitions  *************/
#define CONTROLPORT 5025 ///< TCP/IP port for the SCPI control connection

#define IPC_FILE "gradiometer_ipc.bin"
#define CHECKSUM_PRIME 4294967291L ///< inspired by: https://en.wikipedia.org/wiki/Fletcher's_checksum. A prime number close to (but smaller than) 2^32 

#include <ctype.h>
#include <inttypes.h>
#include <stdint.h>
#include <sys/time.h>
#include <time.h>

#define RINGBUFFERSIZE   (1024*16)
#define MAXCHAN   (8)


#define MAX_LEN 4096 

typedef struct
{
  double goalposition;
  double tacc;
  double maxvel;
  double posthreshold;
  double kp;
  double ti;
  double offset;
  
  double kp_p[8];
  double ki_p[8];  
  
  int64_t trigger;
  char filename[MAX_LEN];
  int64_t currentrun;
  
} config_data_t;


typedef struct
{
  int64_t initstate;
  int64_t movestate;
  int64_t samplecounter;
  int64_t loopcounter;

  double velocity;
  double targetvelocity;

  double posdiff;

  double motorposition;
  double encoderposition;

  double goalposition;
  double a;
  double maxvel;
  double posthreshold;
  double kp;
  double ti;
  int64_t trigger;
  double correction_voltage_coil2;
  double correction_voltage_coil4;
  double read_in_one;
  double read_in_two;
  double read_in_four;

} feedback_data_t;


typedef struct
{
  uint64_t timestamp;
  double  values[MAXCHAN];
} sampledata_t;




typedef struct
{
  uint64_t versionchecksum;
  config_data_t config_data;
  feedback_data_t feedback_data;
  uint64_t cdchecksum;
  uint64_t fdchecksum;
  sampledata_t samplebuffer[RINGBUFFERSIZE];
} ipc_map_t;


typedef struct
{
  int ipc_fd;             ///< IPC file descriptor
  ipc_map_t *map;         ///< IPC memory map
  feedback_data_t
  fbd;   ///< IPC data structure containing the last valid feedback data from the backend
  config_data_t
  cfd;     ///< IPC data structure containing the valid control data to be sent to the backend
} ipc_workdata_t;


uint64_t Fletcher64( void *data, int bytecount );
double dtime(void);
int create_listen_tcp_socket (int port);

int IPC_init (ipc_workdata_t *pwd, int create);
void IPC_write_fbd (ipc_workdata_t *pwd);
int IPC_read_fbd (ipc_workdata_t *pwd);
void IPC_write_cfd (ipc_workdata_t *pwd);
int IPC_read_cfd (ipc_workdata_t *pwd);


#define HOME           printf("\033[1;1H")
#define STORE_CURSOR   printf("\033[s")
#define RESET_CURSOR   printf("\033[u")
#define NORMAL         printf("\033[0m")
#define CLEAR          printf("\033[2J\033[1;1H")
#define HIDECURSOR     printf("\033[?25l")
#define SHOWCURSOR     printf("\033[?25h")
#define CLEARLINE      printf("\033[2K")
#define CURSORUP(N)    printf("\033[%dA",N)
#define CURSORDOWN(N)  printf("\033[%dB",N)
#define CURSORFORW(N)  printf("\033[%dC",N)
#define CURSORBACK(N)  printf("\033[%dD",N)


#define CURSOR(zeile,spalte)  printf("\033[%02d;%02dH", zeile, spalte)
#define COLOR(bg, fg)         printf("\033[%02d;%02dm", bg, fg)
#define COLOUR(bg, fg, fmt)   printf("\033[%02d;%02d;%1dm", bg, fg, fmt)




#endif
