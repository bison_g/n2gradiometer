#include "gradiometer_ipc.h"
#include <unistd.h>
#include <poll.h>
#include <stdio.h>
#include <math.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include "PID.h"
#include "PID.c"

//using namespace std;


#define LOGGINGINTERVAL 1.0



static int doLoop = 1;

void signal_handler(int signum)
{
  switch (signum)
  {
  case  SIGKILL:
  case  SIGINT:
    doLoop = 0;
    break;
  }
}

int main(int argc, char** argv)
{
  int c;
  ipc_workdata_t workdata;
  ipc_workdata_t* pwd = &workdata;

  int64_t oldgoal = 0;
  int64_t newgoal = 0;

  double goal = 0;
  double speed = 0;
  int setgoal = 0;
  int doloops = 0;

  int exit = 0;
  int sequence = 0;

  int loops = 0;
  int retval;


  IPC_init(pwd, 0);
  IPC_read_cfd(pwd);

  if (argc > 1)
  {
    while ((c = getopt (argc, argv, "g:s:ef:l:")) != -1)
    {
      switch (c)
      {
      case 'g':
        goal = atof(optarg);
        setgoal = 1;
        newgoal = goal * 2097152;
        break;

      case 'l':
        loops = atof(optarg);
        doloops = 1;
        break;

      case 's':
        speed = atof(optarg);
        if(speed > 0. && speed <= 1.0)
        {
          pwd->cfd.maxvel = speed * 5000000;
          IPC_write_cfd(pwd);
        }
        else
        {
          printf("speed must be larger than 0 and not larger then 1.0\n");
          return 1;
        }
        break;
      case 'e':
        exit = 1;
        break;

      case 'f':
        printf("filename: %s", optarg);
        strncpy(pwd->cfd.filename, optarg, MAX_LEN);
        IPC_write_cfd(pwd);
        break;

      case '?':
        printf("servo_manual_frontend  -g <goal position, 1.0 = full rotation>  -s <speed, 1.0 = max speed> -e  \n");
        return 1;
      }
    }
  }



  fd_set rfds;
  struct timeval tv;
//  int retval;
  char buff[255] = {0};


  signal(SIGINT, signal_handler);   // catch CRTL+C
  signal(SIGTERM, signal_handler);


  printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");

  int64_t counter = 0;
  int64_t exitcounter = 0;

  //int64_t oldstate = 0;

//  int triggered = 0;

  do
  {
    counter++;

    /* Watch stdin (fd 0) to see when it has input. */
    FD_ZERO(&rfds);
    FD_SET(0, &rfds);

    if(counter >= 20 && pwd->fbd.movestate == 0 && setgoal)
    {
      if(fabs(pwd->cfd.goalposition - newgoal) < 10000 && exit == 1)
      {
        exit = 2;
      }

      oldgoal = pwd->cfd.goalposition;
      pwd->cfd.goalposition = newgoal;

      if (doloops != 0)
      {
        doloops = 2;
        newgoal = oldgoal;
      }

      IPC_write_cfd(pwd);
      setgoal = 0;
    }


    /* Wait up to 50ms. */
    tv.tv_sec = 0;
    tv.tv_usec = 50000;

    retval = select(1, &rfds, NULL, NULL, &tv);

    if (retval > 0 && FD_ISSET(0, &rfds))
    {
      char ch;
      float fval;
      //int dval;
      // Read data from stdin using fgets.
      fgets(buff, sizeof(buff), stdin);


      ch = buff[0];
	  //char parameter[];


      CLEARLINE;
      printf("select parameter:  (g)oal... ");
      scanf("%c", &ch);

      CLEARLINE;
      printf("\n");
      CLEARLINE;

      switch(ch)
      {
      case 'g':
      case 'G':
        printf("goal position is %f, new value: ", pwd->cfd.goalposition / 2097152.);
        if (scanf("%f", &fval))
        {
          pwd->cfd.goalposition = fval * 2097152.;
          IPC_write_cfd(pwd);
        }
        break;

      case 'p':
      case 'P':
        printf("proportianal gain is %f, new value: ", pwd->cfd.kp);
        if (scanf("%f", &fval))
        {
          pwd->cfd.kp = fval;
          IPC_write_cfd(pwd);
        }
        break;

      case 'i':
      case 'I':
        printf("integral time is %f, new value: ", pwd->cfd.ti);
        if (scanf("%f", &fval))
        {
          pwd->cfd.ti = fval;
          IPC_write_cfd(pwd);
        }
        break;

      case 't':
      case 'T':
        printf("position theshold is %f, new value: ", pwd->cfd.posthreshold);
        if (scanf("%f", &fval))
        {
          pwd->cfd.posthreshold = fval;
          IPC_write_cfd(pwd);
        }
        break;


      case 'm':
      case 'M':
        printf("max speed is %f, new value: ", pwd->cfd.maxvel);
        if (scanf("%f", &fval))
        {
          pwd->cfd.maxvel = fval;
          IPC_write_cfd(pwd);
        }
        break;


      case 'a':
      case 'A':
        printf("acceleration time is %f, new value: ", pwd->cfd.tacc);
        if (scanf("%f", &fval))
        {
          pwd->cfd.tacc = fval;
          IPC_write_cfd(pwd);
        }
        break;

      case 'o':
      case 'O':
        printf("offset is %f, new value: ", pwd->cfd.offset);
        if (scanf("%f", &fval))
        {
          pwd->cfd.offset = fval;
          IPC_write_cfd(pwd);
        }
        break;
		
		
	  //Not possible because switch only allows single letter cases --> how should I do that?
	  // maybe implement cases 1,2,3,4,5,6,7 for the different cells and then change their pid settings??
	  /*
	  case 'kp1': 
      case 'Kp1':
        printf("The value of Kp for Coil 1 is %f, new value: ", pwd->cfd.Kp[0]);
        if (scanf("%f", &fval))
        {
          pwd->cfd.Kp[0] = fval;
          IPC_write_cfd(pwd);
        }
        break;*/
		
	 
      /*case '2':
		
		
		
		char parameter[0];
		printf("Which parameter of PID 2 would you like to change?");
		scanf("%s", parameter);
		
		if(parameter == "P" || parameter == "p"){
			
			printf("The proportional gain is %char, new value: ", pid->Kp[3]);
		}
		
        if (scanf("%f", &fval))
        {
          pwd->cfd.offset = fval;
          IPC_write_cfd(pwd);
        }
        break;*/



      }
    }




    CURSORUP(16);

    CLEARLINE;
    printf("\n");
    CLEARLINE;
    printf("\n");
    CLEARLINE;
    printf("\n");


    IPC_read_fbd(pwd);

    if (sequence == 0 && pwd->fbd.movestate != 0)
    {
      sequence = 1;
    }

    if (sequence == 1 && pwd->fbd.movestate == 0 )
    {
      if (doloops == 2 && loops >= 1)
      {
        setgoal = 1;
        loops--;
        sequence = 0; 
      }
      if (loops < 1)
      {
        sequence = 2; 
      }
    }

    if (sequence == 2 && pwd->fbd.movestate == 0 && fabs(pwd->fbd.posdiff) < 5)
    {
      exitcounter ++;
    }

    if (exitcounter > 50)
    {
      doLoop = 0;
      strncpy(pwd->cfd.filename, "", MAX_LEN);
      IPC_write_cfd(pwd);
    }

    CLEARLINE;
    printf("Motor position  : %12.0f \n",  pwd->fbd.motorposition);
    CLEARLINE;
    printf("Encoder position: %12.0f \n",  pwd->fbd.encoderposition);
    CLEARLINE;
    printf("Goal            : %12.0f (%12.0f)\n",  pwd->fbd.goalposition, pwd->cfd.goalposition);
    CLEARLINE;
    printf("Position diff   : %12.0f \n",  pwd->fbd.posdiff);
    CLEARLINE;
    printf("Target velocity : %12.0f \n",  pwd->fbd.targetvelocity);
    CLEARLINE;
    printf("Actual velocity : %12.0f \n",  pwd->fbd.velocity);
    CLEARLINE;
    printf("Proportinal gain: %12.0f \n",  pwd->fbd.kp);
    CLEARLINE;
    printf("Integral time   : %12.0f \n",  pwd->fbd.ti);
    CLEARLINE;
    printf("Pos theshold    : %12.0f \n",  pwd->fbd.posthreshold);
    CLEARLINE;
    printf("Max velocity    : %12.0f \n",  pwd->fbd.maxvel);
    CLEARLINE;
    printf("Acceleration    : %12.0f (%f)\n",  pwd->fbd.a, pwd->fbd.maxvel / pwd->fbd.a);

    CLEARLINE;
    printf("Move state      : %12ld s:%d dl:%d lo:%d\n",  pwd->fbd.movestate, sequence, doloops, loops);


    CLEARLINE;
    printf("Press Enter to edit parameters and CRTL-C to exit\n");

  }
  while (doLoop);

  pwd->cfd.trigger = 0;
  IPC_write_cfd(pwd);
  printf("\n");

  return 0;
}

